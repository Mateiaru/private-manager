# Private Manager
## How to use
Private Manager comes built in a jar, and is ready to use on any pc with at Java 8+ installed.
## Building
This repository provides all files needed, in order to build the source code. The project is maintained and developed in IntelliJ IDEA and you should use it to build the source code (community edition, which is free, works perfectly).
### External Resources
* [ini4j](http://ini4j.sourceforge.net/)