package com.cherrybrand.privatemanager;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

class TreeDialog {

    private static final Image file, folder;

    static {
        file = new Image(TreeDialog.class.getResourceAsStream("img/file.png"), 22, 0, true, true);
        folder = new Image(TreeDialog.class.getResourceAsStream("img/folder.png"), 22, 0, true, true);
    }

    static abstract class TreeNode {

        private Item item;
        final List<TreeNode> children = new ArrayList<>();

        void add(TreeNode child) {
            children.add(child);
        }

        abstract void sort();

        abstract TreeNode createNewRoot();

        abstract boolean isFolder();

        abstract String getTitle();

        abstract TreeNode duplicate();

        @SuppressWarnings("unchecked")
        <T extends TreeNode> List<T> getChildren() {
            return (List<T>) children;
        }

        private Item getItem() {
            if (item != null) return item;
            else return item = new Item(this);
        }

        private static class Item extends TreeItem<TreeNode> {

            private final CheckBox checkBox;

            private Item(TreeNode node) {
                super(node);
                // Init checkbox
                checkBox = new CheckBox(node.getTitle());
                if (node.isFolder()) {
                    checkBox.setGraphic(new ImageView(TreeDialog.folder));
                } else {
                    checkBox.setGraphic(new ImageView(TreeDialog.file));
                }
                checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
                    Item parent = (Item) getParent();
                    if (parent != null) parent.updateSelection();
                    updateChildren(newValue);
                });
                checkBox.indeterminateProperty().addListener((observable, oldValue, newValue) -> {
                    Item parent = (Item) getParent();
                    if (parent != null) parent.updateSelection();
                });
                // Init children
                List<TreeNode.Item> children = new ArrayList<>(node.children.size());
                node.sort();
                for (TreeNode n : node.children) children.add(n.getItem());
                getChildren().setAll(children);
            }

            private void updateChildren(boolean selected) {
                for (TreeItem<TreeNode> item : getChildren()) {
                    Item i = (Item) item;
                    if (i.checkBox.isIndeterminate() || i.checkBox.isSelected() != selected) {
                        i.checkBox.setSelected(selected);
                        i.checkBox.setIndeterminate(false);
                        i.updateChildren(selected);
                    }
                }
            }

            /**
             * 0 = deselected
             * 1 = in between
             * 2 = selected
             */
            private int getSelection() {
                if (checkBox.isIndeterminate()) return 1;
                else if (checkBox.isSelected()) return 2;
                else return 0;
            }

            private void updateSelection() {
                int count = 0, max = getChildren().size() * 2, selection = getSelection(), newSelection;
                for (TreeItem<TreeNode> item1 : getChildren()) count += ((Item) item1).getSelection();
                if (count == 0) newSelection = 0;
                else if (count < max) newSelection = 1;
                else newSelection = 2;
                if (selection != newSelection) {
                    switch (newSelection) {
                        case 0:
                            checkBox.setIndeterminate(false);
                            checkBox.setSelected(false);
                            break;
                        case 1:
                            checkBox.setIndeterminate(true);
                            break;
                        case 2:
                            checkBox.setIndeterminate(false);
                            checkBox.setSelected(true);
                            break;
                    }
                }
            }
        }
    }

    static class TreeNodeFile extends TreeNode implements Comparable<TreeNodeFile> {

        private final Manager.File file;

        TreeNodeFile(Manager.File file) {
            this.file = file;
        }

        void sort() {
            children.sort(null);
        }

        @Override
        TreeNode createNewRoot() {
            return new TreeNodeFile(null);
        }

        Manager.File getFile() {
            return file;
        }

        @Override
        boolean isFolder() {
            return file == null || file instanceof Manager.Folder;
        }

        @Override
        String getTitle() {
            return file == null ? "All" : file.name;
        }

        @Override
        TreeNode duplicate() {
            return new TreeNodeFile(file);
        }

        @Override
        public int compareTo(TreeNodeFile o) {
            return file.compareTo(o.file);
        }

    }

    static class TreeNodePath extends TreeNode implements Comparable<TreeNodePath> {

        private final Path path;
        private final String filename;
        private final boolean isFolder;

        TreeNodePath() {
            this(null, "All", true);
        }

        TreeNodePath(Path path) {
            this.path = path;
            filename = path.getFileName().toString();
            isFolder = Files.isDirectory(path);
        }

        private TreeNodePath(Path path, String filename, boolean isFolder) {
            this.path = path;
            this.filename = filename;
            this.isFolder = isFolder;
        }

        Path getPath() {
            return path;
        }

        void sort() {
            children.sort(null);
        }

        @Override
        TreeNode createNewRoot() {
            return new TreeNodePath();
        }

        @Override
        boolean isFolder() {
            return isFolder;
        }

        @Override
        String getTitle() {
            return filename;
        }

        @Override
        TreeNode duplicate() {
            return new TreeNodePath(path, filename, isFolder);
        }

        @Override
        public int compareTo(TreeNodePath o) {
            return filename.compareTo(o.filename);
        }
    }

    @SuppressWarnings("unchecked")
    static <T extends TreeNode> T show(Stage owner, T root, boolean allowEmptyFolders) {
        Stage stage = Dialogs.stage("Select files", owner);
        stage.setMinWidth(400);
        stage.setMinHeight(300);
        stage.setResizable(true);
        FXMLLoader loader = new FXMLLoader(FileSelector.class.getResource("TreeDialog.fxml"));
        TreeDialog controller = new TreeDialog(stage, root, allowEmptyFolders);
        loader.setController(controller);
        Scene scene;
        try {
            scene = new Scene(loader.load());
        } catch (Exception e) {
            throw new Error(e);
        }
        stage.setScene(scene);
        stage.showAndWait();
        return (T) controller.result;
    }

    @FXML
    private TreeView<TreeNode> treeView;

    private final Stage stage;
    private final TreeNode root, result;
    private final boolean allowEmptyFolders;

    private TreeDialog(Stage stage, TreeNode root, boolean allowEmptyFolders) {
        this.stage = stage;
        this.root = root;
        this.result = root.createNewRoot();
        this.allowEmptyFolders = allowEmptyFolders;
    }

    @FXML
    public void initialize() {
        treeView.setCellFactory(param -> new CustomTreeCell());
        TreeNode.Item root = this.root.getItem();
        treeView.setRoot(root);
        root.checkBox.setSelected(true);
        root.setExpanded(true);
    }

    @FXML
    public void ok() {
        // Add all selected files recursively
        addFiles((TreeNode.Item) treeView.getRoot(), result);
        stage.close();
    }

    // Add all selected children of from into to (recursively)
    private void addFiles(TreeNode.Item from, TreeNode to) {
        // For each child
        for (TreeItem<TreeNode> item1 : from.getChildren()) {
            // Get the item
            TreeNode.Item item = (TreeNode.Item) item1;
            // Skip if item is not selected
            if (!item.checkBox.isSelected()) continue;
            // Create a new node
            TreeNode node = item.getValue().duplicate();
            // If the child is a folder, add all of its children to the new node
            if (item.getValue().isFolder()) {
                addFiles(item, node);
                // If nothing was added, it's an empty folder, don't add it (except if allowing empty folders)
                if (!node.children.isEmpty() || allowEmptyFolders) to.add(node);
            } else to.add(node);
        }
    }

    @FXML
    public void cancel() {
        stage.close();
    }

    private static class CustomTreeCell extends TreeCell<TreeNode> {

        @Override
        protected void updateItem(TreeNode item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) setGraphic(null);
            else setGraphic(item.getItem().checkBox);
        }
    }

}
