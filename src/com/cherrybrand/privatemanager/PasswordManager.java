package com.cherrybrand.privatemanager;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PasswordManager {


    static void show(Stage owner, Manager manager) {
        Stage stage = Dialogs.stage("Password Manager", owner);
        stage.setMinWidth(400);
        stage.setMinHeight(300);
        stage.setResizable(true);
        FXMLLoader loader = new FXMLLoader(PasswordManager.class.getResource("PasswordManager.fxml"));
        loader.setController(new PasswordManager(stage, manager));
        Scene scene;
        try {
            scene = new Scene(loader.load());
        } catch (IOException e) {
            throw new Error(e);
        }
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public TableView<PasswordEntry> table;
    @FXML
    public TableColumn<PasswordEntry, String> columnWebsite, columnUsername, columnPassword;
    @FXML
    public TableColumn<PasswordEntry, LocalDateTime> columnLastModified;
    @FXML
    public ToggleButton toggleShow;
    @FXML
    public TextField search;
    private ObservableList<PasswordEntry> passwordEntries;
    // This allows for fast checking of already existing entries, as well knowing all already added websites
    private Map<String, List<String>> websiteUsernameMap = new HashMap<>();

    private final Stage stage;
    private final Manager manager;
    private TableView.TableViewSelectionModel<PasswordEntry> tableSelection;

    private PasswordManager(Stage stage, Manager manager) {
        this.stage = stage;
        this.manager = manager;
        cellMenuHandler = event -> {
            ClipboardContent content;
            switch (((MenuItem) event.getTarget()).getId()) {
                case "edit":
                    PasswordEntry old = tableSelection.getSelectedItem();
                    PasswordEntry entry = Dialogs.editEntry(stage, old, websiteUsernameMap.keySet(), this::exists);
                    if (entry != null) {
                        entry.lastModified = LocalDateTime.now();
                        passwordEntries.remove(old);
                        passwordEntries.add(entry);
                        List<String> list = websiteUsernameMap.get(old.website);
                        list.remove(old.username);
                        if (old.website.equals(entry.website)) list.add(entry.username);
                        else {
                            if (list.isEmpty()) websiteUsernameMap.remove(old.website);
                            websiteUsernameMap.computeIfAbsent(entry.website, k -> new ArrayList<>()).add(entry.username);
                        }
                        refresh(true);
                    }
                    break;
                case "delete":
                    removeSelected();
                    break;
                case "copyUsername":
                    content = new ClipboardContent();
                    content.putString(tableSelection.getSelectedItem().username);
                    Clipboard.getSystemClipboard().setContent(content);
                    break;
                case "copyPassword":
                    content = new ClipboardContent();
                    content.putString(tableSelection.getSelectedItem().password);
                    Clipboard.getSystemClipboard().setContent(content);
                    break;
            }
        };

    }

    @SuppressWarnings("unchecked")
    @FXML
    public void initialize() {
        // Make the toggleShow button show / hide the passwords table column
        toggleShow.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue && Dialogs.areYouSure(stage, "Are you sure?", "Are you sure you want to show your passwords?", 1, -1) != 1) {
                toggleShow.setSelected(false);
                return;
            }
            if (table.getEditingCell() != null) table.edit(-1, null);
            columnPassword.setVisible(newValue);
            // Reset search because search contains passwords only when they are visible
            refresh(false);
        });
        // Initialize table
        tableSelection = table.getSelectionModel();
        tableSelection.setSelectionMode(SelectionMode.MULTIPLE);
        columnWebsite.setCellFactory(param -> new ComboBoxCell());
        columnUsername.setCellFactory(param -> new TextFieldCell(false));
        columnPassword.setCellFactory(param -> new TextFieldCell(true));
        columnLastModified.setCellFactory(param -> new DateTimeCell());
        // columnLastModified doesn't have a custom cell factory
        // Initialize cell context menu
        MenuItem item = new MenuItem("Edit");
        item.setId("edit");
        item.setOnAction(cellMenuHandler);
        cellContextMenu.getItems().add(item);
        item = new MenuItem("Delete");
        item.setId("delete");
        item.setOnAction(cellMenuHandler);
        cellContextMenu.getItems().add(item);
        /* Separator */
        cellContextMenu.getItems().add(new SeparatorMenuItem());
        item = new MenuItem("Copy username");
        item.setId("copyUsername");
        item.setOnAction(cellMenuHandler);
        cellContextMenu.getItems().add(item);
        item = new MenuItem("Copy password");
        item.setId("copyPassword");
        item.setOnAction(cellMenuHandler);
        cellContextMenu.getItems().add(item);
        // Initialize items & search bar
        table.getItems().setAll(passwordEntries = manager.currentUser.passwordEntries);
        for (PasswordEntry entry : passwordEntries) websiteUsernameMap.computeIfAbsent(entry.website, k -> new ArrayList<>()).add(entry.username);
        search.textProperty().addListener((observable, oldValue, newValue) -> search(newValue));
        // Un-focus search bar when window first appears
        stage.showingProperty().addListener(observable -> table.requestFocus());
    }

    private final EventHandler<ActionEvent> cellMenuHandler;

    @FXML
    public void createEntry() {
        PasswordEntry entry = Dialogs.editEntry(stage, null, websiteUsernameMap.keySet(), this::exists);
        if (entry != null) {
            entry.lastModified = LocalDateTime.now();
            passwordEntries.add(entry);
            if (search.getText() == null || search.getText().isEmpty() || entryMatches(entry, search.getText().split(" "))) {
                table.getItems().add(entry);
                table.sort();
            }
            websiteUsernameMap.computeIfAbsent(entry.website, k -> new ArrayList<>()).add(entry.username);
        }
    }

    private boolean exists(String website, String username) {
        List<String> list = websiteUsernameMap.get(website);
        return list != null && list.contains(username);
    }

    private void refresh(boolean evenIfNoQuery) {
        if (evenIfNoQuery || (search.getText() != null && !search.getText().isEmpty())) search(search.getText());
    }

    @FXML
    public void search(String query) {
        if (query == null || query.isEmpty()) table.getItems().setAll(passwordEntries);
        else {
            String[] q = query.split(" ");
            List<PasswordEntry> list = new ArrayList<>();
            for (PasswordEntry entry : passwordEntries) if (entryMatches(entry, q)) list.add(entry);
            table.getItems().setAll(list);
        }
        table.sort();
    }

    private boolean entryMatches(PasswordEntry entry, String[] query) {
        for (String string : query)
            if (!entry.website.contains(string) && !entry.username.contains(string) && (!toggleShow.isSelected() || !entry.password.contains(string))) return false;
        return true;
    }

    @FXML
    public void removeSelected() {
        if (Dialogs.areYouSure(stage, "Are you sure?", "Are you sure you want to remove all selected items?", 1, -1) == 1) remove(tableSelection.getSelectedItems());
    }

    @FXML
    public void removeShown() {
        if (Dialogs.areYouSure(stage, "Are you sure?", "Are you sure you want to remove all items currently shown?", 1, -1) == 1) remove(table.getItems());
    }

    private void remove(List<PasswordEntry> toRemove) {
        for (PasswordEntry entry : toRemove) {
            passwordEntries.remove(entry);
            websiteUsernameMap.get(entry.website).remove(entry.username);
        }
        refresh(true);
    }

    public static class PasswordEntry {

        private String website, username, password;
        private LocalDateTime lastModified;

        PasswordEntry() {
        }

        PasswordEntry(String website, String username, String password, LocalDateTime lastModified) {
            this.website = website;
            this.username = username;
            this.password = password;
            this.lastModified = lastModified;
        }

        @FXML
        public String getWebsite() {
            return website;
        }

        @FXML
        public String getUsername() {
            return username;
        }

        @FXML
        public String getPassword() {
            return password;
        }

        @FXML
        public LocalDateTime getLastModified() {
            return lastModified;
        }

        void setWebsite(String website) {
            this.website = website;
        }

        void setUsername(String username) {
            this.username = username;
        }

        void setPassword(String password) {
            this.password = password;
        }
    }

    private final ContextMenu cellContextMenu = new ContextMenu();

    private abstract class CustomCell extends TableCell<PasswordEntry, String> {

        private String oldValue;
        private Node node;
        private final boolean allowEmpty;

        private CustomCell(boolean allowEmpty) {
            this.allowEmpty = allowEmpty;
            setOnMouseClicked(event -> {
                if (isEmpty()) {
                    table.edit(-1, null);
                    event.consume();
                }
            });
        }

        protected abstract Node createNode();

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            setText(oldValue);
            setGraphic(null);
        }

        @Override
        public void startEdit() {
            super.startEdit();
            oldValue = getText();
            setText(null);
            setGraphic(getNode());
            node.requestFocus();
        }

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) setContextMenu(null);
            else setContextMenu(cellContextMenu);
            setText(item);
        }

        Node getNode() {
            if (node == null) {
                node = createNode();
                node.setOnKeyReleased(event -> {
                    if (event.getCode() != KeyCode.ESCAPE) return;
                    cancelEdit();
                    event.consume();
                });
            }
            return node;
        }

        @Override
        public void commitEdit(String newValue) {
            if (oldValue.equals(newValue)) {
                cancelEdit();
                return;
            }
            if (!allowEmpty && (newValue == null || newValue.isEmpty())) {
                Dialogs.message(stage, "This value may not be empty", "Please type a value and try again.");
                return;
            }
            if (getTableColumn() != columnPassword) {
                String website, username;
                if (getTableColumn() == columnWebsite) {
                    website = newValue;
                    username = ((PasswordEntry) getTableRow().getItem()).username;
                } else {
                    website = ((PasswordEntry) getTableRow().getItem()).website;
                    username = newValue;
                }
                if (exists(website, username)) {
                    Dialogs.message(stage, "Already exists", "This website / username combination already exists.");
                    return;
                }
            }
            super.commitEdit(newValue);
            setGraphic(null);
            setProperty(((PasswordEntry) getTableRow().getItem()), newValue);
            manager.currentUser.savePasswords();
        }

        private void setProperty(PasswordEntry entry, String value) {
            entry.lastModified = LocalDateTime.now();
            if (getTableColumn() == columnPassword) {
                entry.setPassword(value);
                return;
            }
            List<String> list = websiteUsernameMap.get(entry.website);
            list.remove(entry.username);
            if (getTableColumn() == columnWebsite) {
                if (list.isEmpty()) websiteUsernameMap.remove(entry.website);
                entry.setWebsite(value);
            } else entry.setUsername(value);
            websiteUsernameMap.computeIfAbsent(entry.website, k -> new ArrayList<>()).add(entry.username);
            table.refresh();
            table.sort();
        }
    }

    private class TextFieldCell extends CustomCell {

        private TextFieldCell(boolean allowEmpty) {
            super(allowEmpty);
        }

        @Override
        protected Node createNode() {
            TextField field = new TextField();
            field.setOnAction(event -> {
                commitEdit(field.getText());
                event.consume();
            });
            return field;
        }

        @Override
        public void startEdit() {
            ((TextField) getNode()).setText(getText());
            super.startEdit();
        }
    }

    private class ComboBoxCell extends CustomCell {

        private ComboBoxCell() {
            super(false);
        }

        @Override
        protected Node createNode() {
            ComboBox<String> comboBox = new ComboBox<>();
            comboBox.setMaxWidth(Double.MAX_VALUE);
            comboBox.setEditable(true);
            comboBox.setOnAction(event -> {
                commitEdit(comboBox.getValue());
                event.consume();
            });
            return comboBox;
        }

        @Override
        public void startEdit() {
            @SuppressWarnings("unchecked") ComboBox<String> comboBox = (ComboBox) getNode();
            comboBox.getItems().setAll(websiteUsernameMap.keySet());
            comboBox.setValue(getText());
            super.startEdit();
        }
    }

    private static class DateTimeCell extends TableCell<PasswordEntry, LocalDateTime> {

        private static final DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);

        @Override
        protected void updateItem(LocalDateTime item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) setText(null);
            else setText(item.format(formatter));
        }
    }

}
