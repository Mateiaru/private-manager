package com.cherrybrand.privatemanager;

import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.util.Pair;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.security.auth.DestroyFailedException;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Manager {

    // Paths to various folders (final)
    private final Path configFolder, logFolder;
    // Main instance used for Main.error
    private final Main main;
    // Stage used to center dialogs
    private final Stage stage;
    // Configuration file
    private final Utils.IniHelper config;


    // Path to various folders
    private Path dataDirectory, dataUsers;
    // List of users & current user
    final ArrayList<User> users = new ArrayList<>();
    User currentUser;

    private static final String
            INI_CONFIG = "Config", INI_CONFIG_FOLDER = "dataFolder",
            INI_WINDOW = "Window", INI_WINDOW_X = "x", INI_WINDOW_Y = "y", INI_WINDOW_WIDTH = "width", INI_WINDOW_HEIGHT = "height",
            INI_DIRS = "DefaultDirs", INI_DIRS_REMOVE = "remove", INI_DIRS_ADD = "add";

    // This should be a singleton
    Manager(Main main, Stage stage) {
        this.main = main;
        this.stage = stage;
        // Set folder locations
        configFolder = Paths.get(System.getenv("localappdata"), "PrivateManager");
        logFolder = configFolder.resolve("logs");
        // Set config file
        config = new Utils.IniHelper(configFolder.resolve("config.ini"));
        // Delete temporary files on exit
        // When this gets called it's already too late to alert the user
        Runtime.getRuntime().addShutdownHook(new Thread(this::cleanupSilent));
    }

    // Initialize the manager, this must be outside constructor as it may call Main.error which then calls Manager.createLog which must be non-null
    void init() {
        // Create config directory
        try {
            Files.createDirectories(configFolder);
        } catch (Exception e) {
            main.error("Unable to create directory: " + configFolder, e);
        }
        // Load configuration file if it exists
        try {
            config.load();
        } catch (Exception e) {
            main.error("Unable to open file: " + config.getPath(), e);
        }
        // Set data directory from the file or the default location if file didn't exist
        String temp = config.get(INI_CONFIG).get(INI_CONFIG_FOLDER);
        if (temp == null) temp = configFolder.toString();
        setDataDirectory(temp);
        // Load users list
        Utils.loadFile(dataUsers, null, data -> {
            for (int userCount = data.readInt(); userCount > 0; userCount--) {
                // Load all user information & create the user instance for each user
                String name = data.readUTF();
                String dir = data.readUTF();
                String hierarchy = data.readUTF();
                String passwords = data.readUTF();
                byte[] password = new byte[data.readInt()];
                if (password.length != data.read(password)) throw new IOException("Corrupted users.dat file");
                byte[] key = new byte[data.readInt()];
                if (key.length != data.read(key)) throw new IOException("Corrupted users.dat file");
                byte[] salt = new byte[data.readInt()];
                if (salt.length != data.read(salt)) throw new IOException("Corrupted users.dat file");
                users.add(new User(name, dir, hierarchy, passwords, password, key, salt));
            }
        }, main, stage, "the users list data file");
    }

    // Method to save the users list & their data
    private void saveUsers() {
        // Save the users list
        Utils.saveFile(dataUsers, null, data -> {
            data.writeInt(users.size());
            for (User user : users) {
                // Each user has a name, a directory, a hierarchy file, a passwords file, a password hash, a secret key and a salt for the password hash
                // This data is not encrypted because 1. there is no key to encrypt it with
                // and 2. it's publicly available by simply running the app or checking the data directory in a file explorer, which is already done if you're opening this file
                data.writeUTF(user.name);
                data.writeUTF(user.dir);
                data.writeUTF(user.hierarchy);
                data.writeUTF(user.passwords);
                data.writeInt(user.passwordHash.length);
                data.write(user.passwordHash);
                data.writeInt(user.keyEncrypted.length);
                data.write(user.keyEncrypted);
                data.writeInt(user.salt.length);
                data.write(user.salt);
            }
        }, main, "the users list data file");
    }

    // Quick utility method
    private void saveConfig() {
        try {
            config.save();
        } catch (Exception e) {
            main.error("Unable to save configuration file: " + config.getPath(), e);
        }
    }

    // Updated the data directory
    private void setDataDirectory(String dataDirectory) {
        // Update the configuration file
        config.get(INI_CONFIG).set(INI_CONFIG_FOLDER, dataDirectory);
        // Don't add multiple times
        this.dataDirectory = Paths.get(dataDirectory);
        this.dataUsers = this.dataDirectory.resolve("users.dat");
        // Notify users to update the directory
        for (User user : users) user.updateDir();
        saveConfig();
    }

    // Methods used to load / save configuration data, called from Main
    void saveWindowLocation(Stage stage) {
        Utils.IniHelper.Section section = config.get(INI_WINDOW);
        section.set(INI_WINDOW_X, String.valueOf(stage.getX()));
        section.set(INI_WINDOW_Y, String.valueOf(stage.getY()));
        section.set(INI_WINDOW_WIDTH, String.valueOf(stage.getWidth()));
        section.set(INI_WINDOW_HEIGHT, String.valueOf(stage.getHeight()));
        saveConfig();
    }

    void loadWindowLocation(Stage stage) {
        Utils.IniHelper.Section section = config.get(INI_WINDOW);
        String temp;
        temp = section.get(INI_WINDOW_X);
        if (temp != null) {
            try {
                stage.setX(Double.parseDouble(temp));
            } catch (NumberFormatException ignored) {
            }
        }
        temp = section.get(INI_WINDOW_Y);
        if (temp != null) {
            try {
                stage.setY(Double.parseDouble(temp));
            } catch (NumberFormatException ignored) {
            }
        }
        temp = section.get(INI_WINDOW_WIDTH);
        if (temp != null) {
            try {
                stage.setWidth(Double.parseDouble(temp));
            } catch (NumberFormatException ignored) {
            }
        }
        temp = section.get(INI_WINDOW_HEIGHT);
        if (temp != null) {
            try {
                stage.setHeight(Double.parseDouble(temp));
            } catch (NumberFormatException ignored) {
            }
        }
    }

    void saveAddDirectory(String dir) {
        config.get(INI_DIRS).set(INI_DIRS_ADD, dir);
        saveConfig();
    }

    String loadAddDirectory() {
        return config.get(INI_DIRS).get(INI_DIRS_ADD);
    }

    void saveRemoveDirectory(String dir) {
        config.get(INI_DIRS).set(INI_DIRS_REMOVE, dir);
        saveConfig();
    }

    String loadRemoveDirectory() {
        return config.get(INI_DIRS).get(INI_DIRS_REMOVE);
    }

    // Logs util
    private static final DateFormat createLogPattern = new SimpleDateFormat("yyyy.MM.dd_HH.mm.s.S");

    Path createLog() throws IOException {
        Files.createDirectories(logFolder);
        String name = "log_" + createLogPattern.format(new Date()) + ".log";
        return logFolder.resolve(name);
    }

    // Used for user's password hashing
    private static final MessageDigest digest;

    static {
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    // User management

    boolean login(String name, String password) {
        // Attempt login with username and password
        User loggedUser = null;
        boolean logged = false;
        for (User user : users) {
            if (user.name.equals(name)) {
                logged = user.login(password);
                loggedUser = user;
                break;
            }
        }
        // Logged = password was correct
        if (!logged) return false;
        currentUser = loggedUser;
        // Notify Main controller (UI) of successful login
        main.login();
        return true;
    }

    private boolean usernameExists(String username) {
        for (User user : users) if (user.name.equalsIgnoreCase(username)) return true;
        return false;
    }

    boolean signup(String name, String password) {
        // Check if username already exists
        if (usernameExists(name)) return false;
        users.add(new User(name, password));
        saveUsers();
        return true;
    }

    int changeUsername(String newName, String password) {
        // First, check password
        if (!currentUser.password.equals(password)) return 1;
        // Check if username already exists
        if (usernameExists(newName)) return 2;
        // Perform name change
        currentUser.setName(newName);
        main.labelUser.setText(newName);
        saveUsers();
        return 0;
    }

    boolean changePassword(String newPassword, String oldPassword) {
        // Check if current password is correct
        if (!currentUser.password.equals(oldPassword)) return false;
        // Set new password
        currentUser.setPassword(newPassword);
        saveUsers();
        return true;
    }

    // Returns whether the password was correct
    boolean deleteAccount(String password) {
        // Check password first
        if (!currentUser.password.equals(password)) return false;
        // Delete all user's files
        try {
            Utils.deleteDirectory(currentUser.userDir);
        } catch (Exception e) {
            main.error("Unable to delete user directory:\n" + currentUser.userDir, e);
            return true;
        }
        // Remove the user from the list & logout
        users.remove(currentUser);
        main.logout();
        saveUsers();
        return true;
    }

    // File / folders methods

    boolean rename(File file, String newName) {
        // Check if filename already exists
        if (main.current.find(newName) != null) return false;
        // If file is opened for renaming, attempt renaming the temp file
        Pair<Boolean, Path> openFile = openFiles.get(file);
        if (openFile != null && Files.exists(openFile.getValue())) {
            Path p = openFile.getValue();
            String[] newSplit = Utils.splitFilename(newName), oldSplit = Utils.splitFilename(file.name);
            String filename = p.getFileName().toString();
            try {
                if (!openFile.getKey()) Utils.setReadOnly(false, p);
                Files.move(p, p = p.resolveSibling(newSplit[0] + filename.substring(oldSplit[0].length(), filename.length() - oldSplit[1].length()) + newSplit[1]));
                openFiles.put(file, new Pair<>(openFile.getKey(), p));
                if (!openFile.getKey()) Utils.setReadOnly(true, p);
            } catch (Exception ignored) {
                Dialogs.message(stage, "Unable to rename temporary file", "Unable to rename open file: " + filename +
                        "\nClose the file, apply changes / cleanup and then open it again.");
            }
        }
        // Rename the file inside the private folder
        file.name = newName;
        currentUser.saveHierarchy();
        main.refreshDisplay();
        return true;
    }

    boolean newFolder(String name) {
        Folder f = main.current;
        // Check if filename already exists
        if (f.find(name) != null) return false;
        f.files.add(new Folder(name, f));
        currentUser.saveHierarchy();
        main.refreshDisplay();
        return true;
    }

    boolean newFile(String name) {
        Folder f = main.current;
        // Check if filename already exists
        if (f.find(name) != null) return false;
        f.files.add(new File(name, currentUser, f));
        currentUser.saveHierarchy();
        main.refreshDisplay();
        return true;
    }

    File deleteFiles(List<File> files) {
        try {
            // Delete a list of files
            for (File f : files) {
                if (!deleteFile(f)) return f; // On error, return the file that wasn't deleted
                    // Remove from parent
                else f.parent.files.remove(f);
            }
            return null;
        } finally {
            // Before returning
            main.refreshDisplay();
            currentUser.saveHierarchy();
        }
    }

    // Deletes a file without removing it from parent
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    boolean deleteFile(File f) {
        // If delete fails, return false
        if (!f.delete()) return false;
        else {
            // If we delete a file that is opened in edit mode, disallow applying changes from it
            Pair<Boolean, Path> openFile = openFiles.get(f);
            if (openFile != null && openFile.getKey()) {
                openFiles.put(f, new Pair<>(false, openFile.getValue()));
                try {
                    Utils.setReadOnly(true, openFile.getValue());
                } catch (Exception ignored) {
                }
            }
            // Delete succeeded
            return true;
        }
    }

    void moveOrCopyFiles(List<File> files, boolean move, Folder from, Folder to) {
        if (from == to && move) {
            Dialogs.message(stage, "Unable to move files", "Destination folder is the same as source folder.");
            return;
        }
        // Check for recursive copying / moving
        for (File f : files) {
            if (f instanceof Folder) {
                if (f == to || checkRecursiveCopy((Folder) f, to)) {
                    Dialogs.message(stage, "Unable to move / copy files", "Attempting to move / copy a folder inside itself (destination folder is a subfolder of source).");
                    return;
                }
            }
        }
        // Copy / move the files
        for (File f : files) {
            if (move) f.move(to);
            else {
                Exception e = f.copy(to);
                if (e != null) {
                    main.error("Unable to copy file: " + f.name, e);
                    return;
                }
            }
        }
        main.refreshDisplay();
        currentUser.saveHierarchy();
        // Allow multiple pastes when copying
        if (move) files.clear(); // 'files' is the clipboard files list
    }

    // Util method
    private boolean checkRecursiveCopy(Folder src, Folder dest) {
        for (File f : src) {
            if (f instanceof Folder) {
                if (f == dest) return true;
                else if (checkRecursiveCopy((Folder) f, dest)) return true;
            }
        }
        return false;
    }

    // List of files that should be deleted on exit
    private final Map<File, Pair<Boolean, Path>> openFiles = new HashMap<>();
    // File file object that was decrypted, whether the file is open in edit mode, the path to the file

    void openFile(File file, boolean editMode) {
        // Check if file is already open
        Path p = null;
        Pair<Boolean, Path> openFile = openFiles.get(file);
        if (openFile != null && Files.exists(openFile.getValue())) {
            // Trying to open in edit mode, file already opened in view mode
            if (editMode && !openFile.getKey()) {
                switch (Dialogs.areYouSure(stage, "File being viewed", "File is already opened in view mode. Would you like to mark the current file as editable (and enable " +
                        "it's apply changes option) instead of deleting the temporary file and re-opening the it in edit mode? Choose yes if you have the file opened in an " +
                        "editor and you are unable to save the changes.", -1, 1)) {
                    case 0:
                        return;
                    case 1: // Make file editable
                        p = openFile.getValue();
                        try {
                            Utils.setReadOnly(false, p);
                        } catch (Exception e) {
                            Dialogs.message(stage, "Error", "Unable to mark file as writable: " + p.getFileName());
                            return;
                        }
                        openFiles.put(file, new Pair<>(true, p));
                        Dialogs.message(stage, "Success", "File is now editable. You may now save it and apply changes when done.");
                        return;
                    case -1: // Re-open file
                        try {
                            Utils.setReadOnly(false, openFile.getValue());
                            Files.delete(openFile.getValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Dialogs.message(stage, "Unable to open", "Cannot delete file opened in view mode: " + openFile.getValue().getFileName() +
                                    ". Please close the file and try again. File must not be opened in view mode to be opened in edit mode.");
                            return;
                        }
                        break;
                }
                // Trying to open in view mode, file already opened in edit mode
            } else if (!editMode && openFile.getKey()) {
                switch (Dialogs.areYouSure(stage, "File being edited", "The specified file is already opened in edit mode. Would you like to save the changes to this file" +
                        "before re-opening it in view mode?", -1, 1)) {
                    case 0:
                        return;
                    case 1:
                        // Save file
                        if (Utils.encryptAsync(currentUser.key, openFile.getValue(), file.getFile(), stage, main)) return;
                        // Flow-through
                    case -1:
                        try {
                            Files.delete(openFile.getValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Dialogs.message(stage, "Unable to open", "Cannot delete file: " + openFile.getValue().getFileName() + ". Please close the file and try again.");
                            return;
                        }
                }
            } else p = openFile.getValue(); // File already opened correctly
        }
        // If file is empty and we're not opening it in edit mode, there's nothing to display
        else if (!editMode && !Files.exists(file.getFile())) {
            Dialogs.message(stage, "File is empty", "The file you are trying to view is empty.");
            return;
        }
        if (p == null) {
            String[] nameSplit = Utils.splitFilename(file.name);
            try {
                // If the file has an extension, the temp file for 'Filename.txt' will be: 'Filename-blablabla.txt'
                // If the file doesn't have an extension, the temp file for 'Filename' will be: 'Filename-blablabla'
                p = Files.createTempFile(nameSplit[0].concat("-"), nameSplit[1]);
            } catch (Exception e) {
                main.error("Unable to create temporary file for " + file.name, e);
                return;
            }
            // Decrypt the file into the temp file
            try {
                if (Files.exists(file.getFile()) && Utils.decryptAsync(currentUser.key, file.getFile(), p, stage, main)) return;
            } catch (SecurityException e) {
                Dialogs.securityException(stage, true, file.getFile() + " OR " + p, e);
                return;
            }
            // If the file is opened in view mode, make it read only
            if (!editMode) {
                try {
                    Utils.setReadOnly(true, p);
                } catch (Exception e) {
                    main.error("Error while making file as read-only: " + p.getFileName(), e);
                }
            }
            // Add the file to list of open files
            openFiles.put(file, new Pair<>(editMode, p));
        }
        // Open the file using the default program (explorer handles this)
        String cmd = "explorer \"" + p + "\"";
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (Exception e) {
            // Should never happen
            main.error("Error while executing: " + cmd, e);
        }
    }

    /**
     * Check if any files are being edited
     *
     * @return whether to continue operation (false if user clicked cancel or clicked yes and changes couldn't be applied)
     */
    boolean checkEditing() {
        boolean found = false;
        for (Pair<Boolean, Path> openFile : openFiles.values()) {
            if (openFile.getKey()) {
                found = true;
                break;
            }
        }
        if (!found) return true;
        switch (Dialogs.areYouSure(stage, "Files being edited", "Would you like to apply changes to files currently being edited? If you choose no, files resulted from this " +
                "operation may not be up to date.", -1, 1)) {
            case 0:
                return false;
            case 1:
                return true;
        }
        return applyEdited();
    }

    // Returns whether should cleanup
    boolean applyEdited() {
        boolean failed = false;
        Iterator<Map.Entry<File, Pair<Boolean, Path>>> iterator = openFiles.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<File, Pair<Boolean, Path>> entry = iterator.next();
            Pair<Boolean, Path> openFile = entry.getValue();
            Path dest = entry.getKey().getFile();
            if (!openFile.getKey()) continue; // Skip if file was opened in view mode
            if (!Files.exists(openFile.getValue())) {
                // If the temporary file doesn't exist, there is nothing to be saved
                iterator.remove();
                continue;
            }
            try {
                // Save the file
                Utils.encryptFile(currentUser.key, openFile.getValue(), dest);
            } catch (Exception e) {
                main.error("Error while encrypting file: " + dest.getFileName(), e);
                failed = true;
            }
            if (!failed) {
                try {
                    Files.delete(openFile.getValue());
                    iterator.remove();
                } catch (Exception e) {
                    Dialogs.message(stage, "Delete failed", "Unable to delete old temporary file after encryption: " + dest.getFileName());
                    failed = true;
                }
            }
        }
        if (failed) {
            Dialogs.message(stage, "Apply couldn't finished", "Some files could not be saved. Close any open files and try again.");
            return false;
        } else return true;
    }

    // Returns whether should exit
    boolean cleanup(boolean exiting) {
        for (Pair<Boolean, Path> openFile : openFiles.values()) {
            if (openFile.getKey()) {
                switch (Dialogs.areYouSure(stage, "Files being edited", "There are files that have been edited and not updated in the private folder. " +
                        "Do you want to save changes before cleaning up? Changes will be lost otherwise.", -1, 1)) {
                    case 1:
                        if (!applyEdited()) return false;
                        break;
                    case 0:
                        return false;
                }
                break;
            }
        }
        // Delete all temporary files
        boolean failed = false;
        Iterator<Pair<Boolean, Path>> iterator = openFiles.values().iterator();
        while (iterator.hasNext()) {
            Path path = iterator.next().getValue();
            try {
                if (Files.exists(path)) {
                    Utils.setReadOnly(false, path);
                    Files.delete(path);
                }
                iterator.remove();
            } catch (Exception e) {
                Dialogs.message(stage, "Delete failed", "Error while deleting file: " + path.getFileName());
                failed = true;
            }
        }
        // If any file was not deleted, alert user
        if (failed) {
            if (exiting) {
                return Dialogs.areYouSure(stage, "Unable to cleanup", "Unable to delete temporary files, close any open files and try again. If app is closed without cleanup, " +
                        "someone else may be able to view your files. Shutdown anyways?", 1, -1) == -1;
            } else Dialogs.message(stage, "Unable to cleanup", "Unable to delete temporary files, close any open files and try again.");
        }
        return exiting;
    }

    void cleanupSilent() {
        for (Pair<Boolean, Path> openFile : openFiles.values()) {
            try {
                if (Files.exists(openFile.getValue())) {
                    Utils.setReadOnly(false, openFile.getValue());
                    Files.delete(openFile.getValue());
                }
            } catch (Exception ignore) {
            }
        }
        openFiles.clear();
    }

    void fullCleanup(Stage owner) {
        new Dialogs.AsyncTask<Integer>(owner, "Cleaning up...", "Cleaning un temporary files folder...", true) {
            @Override
            Integer run() {
                int deleted = 0;
                try (Stream<Path> stream = Files.list(Paths.get(System.getenv("localappdata"), "Temp"))) {
                    List<Path> list = stream.filter(path -> {
                        try {
                            return Files.isRegularFile(path);
                        } catch (Exception e) {
                            return false;
                        }
                    }).collect(Collectors.toList());
                    for (Path p : list) {
                        try {
                            Files.delete(p);
                        } catch (IOException ignored) {
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
                return deleted;
            }

            @Override
            void afterRun(Integer result) {
                if (isCancelled()) Dialogs.message(stage, "Cancelled", "Cleanup cancelled by user, " + result + " files were deleted.");
                else if (result == null) Dialogs.message(stage, "Error", "Unable to cleanup. Please retry or delete the files inside the '%temp%' folder manually.");
                else Dialogs.message(owner, "Complete", "Cleanup complete. " + result + " files were deleted.");
            }
        }.start(false);
    }

    void decryptFile(Path dest, File file) throws Exception {
        if (Files.exists(file.getFile())) Utils.decryptFile(currentUser.key, file.getFile(), dest);
        else Files.createFile(dest);
    }

    void encryptFile(File dest, Path path) throws Exception {
        Utils.encryptFile(currentUser.key, path, dest.getFile());
    }

    String[] getOpenFiles() {
        StringBuilder viewing = new StringBuilder();
        StringBuilder editing = new StringBuilder();
        for (Map.Entry<File, Pair<Boolean, Path>> entry : openFiles.entrySet()) {
            if (entry.getValue().getKey()) editing.append(entry.getKey().getPrivatePath()).append('\n');
            else viewing.append(entry.getKey().getPrivatePath()).append('\n');
        }
        if (viewing.length() > 0) viewing.setLength(viewing.length() - 1);
        if (editing.length() > 0) editing.setLength(editing.length() - 1);
        return new String[]{viewing.toString(), editing.toString()};
    }

    class User {

        // User's name, may be changed
        String name;
        // Users's password, only exists while logged in | User's data directory name
        private String password;
        // The name of the file containing the file hierarchy
        private final String dir, hierarchy, passwords;
        // The Folder instance representing the root of the private folder
        final Folder root = new Folder("root", null);
        // The passwords in the user's password manager
        final ObservableList<PasswordManager.PasswordEntry> passwordEntries = FXCollections.observableArrayList();
        private final InvalidationListener passwordEntriesListener = observable -> savePasswords();
        // The path to the user directory (the path to the directory named dir)
        private Path userDir;
        // The hash used to check password, the encrypted key, the salt used in password hashing
        private byte[] passwordHash, keyEncrypted, salt;
        // The key used to encrypt all files, exists so that if password changes files don't need to be re-encrypted with the new password
        private SecretKey key;

        // Constructor to generate a new user
        private User(String name, String password) {
            this.name = name;
            // Generate a new key for file encryption and a salt
            try {
                KeyGenerator generator = KeyGenerator.getInstance("AES");
                generator.init(Utils.MAX_KEY_SIZE);
                key = generator.generateKey();
            } catch (NoSuchAlgorithmException e) {
                throw new Error(e);
            }
            this.salt = Utils.salt();
            // Store the password & generate the password hash
            setPassword(password);
            // Obtain a directory for the user data and set paths accordingly
            String dir;
            do dir = Utils.nextString(); while (Files.exists(dataDirectory.resolve(dir)));
            this.dir = dir;
            updateDir();
            // Obtain hierarchy and password files names
            this.hierarchy = Utils.nextString().concat(".pmr");
            String passwords;
            do passwords = Utils.nextString().concat(".pmr"); while (passwords.equals(hierarchy)); // Is this even required? Chances are like, 1 in 10^a lot
            this.passwords = passwords;
        }

        // Constructor to load an existing user (data comes from the disk)
        private User(String name, String dir, String hierarchy, String passwords, byte[] passwordHash, byte[] key, byte[] salt) {
            this.name = name;
            this.dir = dir;
            this.hierarchy = hierarchy;
            this.passwords = passwords;
            updateDir();
            this.passwordHash = passwordHash;
            keyEncrypted = key;
            this.salt = salt;
        }

        // Generate a password hash (hashes a salt & the username too in order to avoid brute forcing or rainbow tables)
        private byte[] digest() {
            digest.update(name.getBytes());
            digest.update(salt);
            digest.update(password.getBytes());
            return digest.digest();
        }

        // Performs all login operations
        private boolean login(String password) {
            // Password needs to be set for digest() to use it
            this.password = password;
            if (Arrays.equals(digest(), passwordHash)) {
                // Decrypt key
                try {
                    key = Utils.decryptKey(keyEncrypted, password);
                } catch (Exception e) {
                    main.error("Unable to decrypt key: ", e);
                    return false;
                }
                // Create user dir
                try {
                    Files.createDirectories(userDir);
                } catch (Exception e) {
                    main.error("Unable to create directory: " + userDir, e);
                    return true;
                }
                // Load hierarchy if it exists, if that doesn't work load from backup
                Utils.loadFile(userDir.resolve(hierarchy), key, data -> {
                    root.files.clear();
                    for (int count = data.readInt(); count > 0; count--) {
                        // Read file path
                        // In our example, hello/world for the first item and file.txt for the second one
                        String file = data.readUTF();
                        // Ensure we have the folder hierarchy for this file
                        // Example: for the 'world' folder, ensure we have the 'hello' folder
                        int start = 0;
                        Folder folder = root;
                        for (int i = 0; i < file.length(); i++) {
                            if (file.charAt(i) == '/') {
                                String s = file.substring(start, i);
                                boolean found = false;
                                for (File f : folder) {
                                    if (f.name.equals(s)) {
                                        found = true;
                                        folder = (Folder) f;
                                        break;
                                    }
                                }
                                if (!found) {
                                    Folder temp = new Folder(s, folder);
                                    folder.files.add(temp);
                                    folder = temp;
                                }
                                start = i + 1;
                            }
                        }
                        // The file's data on the disk (if empty it's a directory, for example - our 'world' directory)
                        String filename = data.readUTF();
                        // Add the file to the correct folder
                        File f;
                        if (filename.isEmpty()) f = new Folder(file.substring(start), folder);
                        else f = new File(filename, file.substring(start), this, folder);
                        folder.files.add(f);
                    }
                }, main, stage, "the user data file");
                // Load the passwords
                Utils.loadFile(userDir.resolve(passwords), key, data -> {
                    passwordEntries.clear();
                    for (int count = data.readInt(); count > 0; count--)
                        passwordEntries.add(new PasswordManager.PasswordEntry(data.readUTF(), data.readUTF(), data.readUTF(), LocalDateTime.parse(data.readUTF())));
                }, main, stage, "the user passwords file");
                passwordEntries.addListener(passwordEntriesListener);
                return true;
            }
            return false;
        }

        void logout() {
            // Remove all sensitive information
            this.password = null;
            try {
                key.destroy();
            } catch (DestroyFailedException ignore) {
                // Don't tell anyone, better hope GC does its job
                // Also should never happen
            }
            key = null;
            root.files.clear();
            passwordEntries.removeListener(passwordEntriesListener);
            passwordEntries.clear();
        }

        private void setName(String name) {
            this.name = name;
            // Password hash depends on username, must be updated
            passwordHash = digest();
        }

        private void setPassword(String password) {
            // Update password & its hash
            this.password = password;
            passwordHash = digest();
            try {
                keyEncrypted = Utils.encryptKey(key, password);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        private void updateDir() {
            userDir = dataDirectory.resolve(dir);
        }

        void saveHierarchy() {
            Utils.saveFile(userDir.resolve(hierarchy), key, data -> {
                data.writeInt(countFiles(root));
                writeFiles(data, "", root);
            }, main, "the user data file");
        }

        // Recursively count files
        private int countFiles(Folder folder) {
            int count = 0;
            for (File file : folder) {
                if (file instanceof Folder) {
                    Folder f = (Folder) file;
                    if (f.files.isEmpty()) count++;
                    else count += countFiles(f);
                } else count++;
            }
            return count;
        }

        // Recursively write files hierarchy
        private void writeFiles(DataOutputStream os, String hierarchy, Folder folder) throws IOException {
            for (File file : folder) {
                if (file instanceof Folder) {
                    Folder f = (Folder) file;
                    if (f.files.isEmpty()) {
                        os.writeUTF(hierarchy.concat(f.name));
                        os.writeUTF("");
                    } else writeFiles(os, hierarchy + f.name + "/", f);
                } else {
                    os.writeUTF(hierarchy.concat(file.name));
                    os.writeUTF(file.filename);
                }
            }
        }

        // Save all passwords entries
        void savePasswords() {
            Utils.saveFile(userDir.resolve(passwords), key, data -> {
                data.writeInt(passwordEntries.size());
                for (PasswordManager.PasswordEntry entry : passwordEntries) {
                    data.writeUTF(entry.getWebsite());
                    data.writeUTF(entry.getUsername());
                    data.writeUTF(entry.getPassword());
                    data.writeUTF(entry.getLastModified().toString());
                }
            }, main, "the user passwords file");
        }

    }

    // Class representing a file folder
    // It has children (files / folder) and no file on the disk
    static class Folder extends File implements Iterable<File> {

        // Children
        final ArrayList<File> files = new ArrayList<>();

        // Don't pass a path / user to the File (super)
        Folder(String name, Folder parent) {
            super(null, name, null, parent);
        }

        // Sort all children based on File.compare
        void sort() {
            files.sort(null);
        }

        // Folders do not have a file on the disk
        @Override
        Path getFile() {
            throw new UnsupportedOperationException("Cannot get path for a folder");
        }

        // Convenience to allow for each on a Folder
        @Override
        public Iterator<File> iterator() {
            return files.iterator();
        }

        // Delete this folder
        @Override
        protected boolean delete() {
            // Try to delete each subfolder / sub-file
            for (Iterator<File> iterator = iterator(); iterator.hasNext(); ) {
                if (iterator.next().delete()) iterator.remove();
            }
            // Deletion succeeded
            if (files.isEmpty()) {
                // This folder may still be in the clipboard, this disables pasting it
                deleted = true;
                return true;
            } else {
                // Deletion failed
                return false;
            }
        }

        // Find a child by its name (ignores case)
        File find(String name) {
            for (File file : files) if (file.name.equalsIgnoreCase(name)) return file;
            return null;
        }

        // Copy this folder and all of its contents to the new root (paste from copy)
        @Override
        Exception copy(Folder newRoot) {
            // If this folder was deleted, don't
            if (deleted) return null;
            // If a folder with the same name exists, add (1), (2), (3) etc. to the name until name is available
            String name = Utils.findFilename(this.name, s -> newRoot.find(s) == null);
            // Create the destination folder
            Folder duplicate = new Folder(name, newRoot);
            newRoot.files.add(duplicate);
            // Copy all subfolders / files to the new folder
            for (File f : this) {
                // In case of error, stop
                Exception e = f.copy(duplicate);
                if (e != null) return e;
            }
            return null;
        }
    }

    static class File implements Comparable<File> {

        // The disk file's name
        private final String filename;
        // This file's name inside the private folder
        String name;
        // The user this file belongs to
        private final User user;
        // The folder this file is placed in
        Folder parent;

        // Load an existing file
        private File(String path, String name, User user, Folder parent) {
            this.filename = path;
            this.name = name;
            this.user = user;
            this.parent = parent;
        }

        // Create a new file
        File(String name, User user, Folder parent) {
            this.name = name;
            this.user = user;
            this.parent = parent;
            String filename;
            // Find a free name for this file
            do filename = Utils.nextString().concat(".pmr"); while (Files.exists(user.userDir.resolve(filename)));
            this.filename = filename;
        }

        // Obtains a path to the disk file
        Path getFile() {
            return user.userDir.resolve(filename);
        }

        // Return size in bytes
        long size() throws IOException {
            if (!Files.exists(getFile())) return 0;
            else return Files.size(getFile());
        }

        // Obtains the full in-private-folder path for this file
        String getPrivatePath() {
            List<String> list = new ArrayList<>();
            list.add(name);
            Folder f = parent;
            while (f.parent != null) {
                list.add(f.name);
                f = f.parent;
            }
            StringBuilder builder = new StringBuilder(list.get(list.size() - 1));
            for (int i = list.size() - 2; i >= 0; i--) builder.append('/').append(list.get(i));
            return builder.toString();
        }

        // Used for copy / pastes
        boolean deleted = false;

        /**
         * @return <code>true</code> if deletion succeeds
         */
        protected boolean delete() {
            try {
                // Delete the disk file if it exists
                Files.deleteIfExists(getFile());
                // Mark as deleted so it can't be pasted
                deleted = true;
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        // Copy this file to a folder (paste from copy)
        Exception copy(Folder newRoot) {
            // If file is deleted, don't
            if (deleted) return null;
            // Find a name for this file if destination already exists
            String name = Utils.findFilename(this.name, s -> newRoot.find(s) == null);
            // Create the file
            File duplicate = new File(name, user, newRoot);
            // Attempt to copy the file on the disk
            try {
                if (Files.exists(getFile())) Files.copy(getFile(), duplicate.getFile());
            } catch (Exception e) {
                return e;
            }
            // Finalize the copy action
            newRoot.files.add(duplicate);
            return null;
        }

        // Move this file to a new folder (paste from cut)
        private void move(Folder newRoot) {
            // Don't if deleted
            if (deleted) return;
            // Remove from parent
            parent.files.remove(this);
            // Update parent reference
            parent = newRoot;
            // Add to new parent
            newRoot.files.add(this);
        }

        // Used to sort files alphabetically, folders go first
        @Override
        public int compareTo(File o) {
            if (this instanceof Folder) return o instanceof Folder ? this.name.compareTo(o.name) : -1;
            else if (o instanceof Folder) return 1;
            else return this.name.compareTo(o.name);
        }
    }

}
