package com.cherrybrand.privatemanager;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Help {

    static void show(Stage owner) {
        Stage stage = Dialogs.stage("Help", owner);
        stage.setMinWidth(400);
        stage.setMinHeight(300);
        stage.setResizable(true);
        Dialogs.scene("Help.fxml", stage);
        stage.show();
    }

    @FXML
    public SplitPane root;
    @FXML
    public TextField search;
    @FXML
    public ListView<Category> listViewCategories;
    @FXML
    public ListView<Pair<Category, TextFlow>> listViewResults;
    @FXML
    public ScrollPane content;

    private static class Category {

        private final String title, index;
        private final Pane pane;
        private static final Font bold = Font.font(null, FontWeight.BOLD, 12d);

        private Category(String title, String file, Consumer<Pane> init) {
            this.title = title;
            StringBuilder builder = new StringBuilder(title);
            builder.append(' ');
            try {
                pane = FXMLLoader.load(Help.class.getResource("help/".concat(file)));
            } catch (Exception e) {
                throw new Error(e);
            }
            for (Node node : pane.getChildren()) {
                if (node instanceof Label) {
                    builder.append(((Label) node).getText());
                    builder.append(' ');
                }
            }
            index = builder.toString();
            if (init != null) init.accept(pane);
        }

        private List<TextFlow> search(String query) {
            int i = 0;
            List<TextFlow> result = new ArrayList<>();
            while (i != -1) {
                TextFlow textFlow = new TextFlow();
                i = index.indexOf(query, i);
                if (i != -1) {
                    int left = Math.max(0, i - 40), right = Math.min(index.length() - 1, i + 40);
                    while (index.charAt(left) == ' ') ++left;
                    while (index.charAt(right) == ' ') --right;
                    ++right;
                    Text text = new Text(index.substring(i, i + query.length()));
                    text.setFont(bold);
                    textFlow.getChildren().addAll(
                            new Text("..."),
                            new Text(index.substring(left, i)),
                            text,
                            new Text(index.substring(i + query.length(), right)),
                            new Text("...")
                    );
                    result.add(textFlow);
                    i = right;
                }
            }
            return result;
        }
    }

    private final Category[] categories = new Category[]{
            new Category("How to use - In a nutshell", "inANutshell.fxml", null),
            new Category("Keyboard Shortcuts", "keyboardShortcuts.fxml", pane -> {
                TableView<Utils.TableItem> table = Utils.createTable("Shortcut", "Usage",
                        new Pair<>("CTRL + C", "copy the selected files to clipboard"),
                        new Pair<>("CTRL + X", "move the selected files to clipboard"),
                        new Pair<>("CTRL + V", "paste files from the clipboard"),
                        new Pair<>("CTRL + N", "create a new file / folder"),
                        new Pair<>("ENTER", "open file as read only / browse folder"),
                        new Pair<>("BACKSPACE", "go up a level"),
                        new Pair<>("HOME", "go the root of your private folder"),
                        new Pair<>("F2", "rename file / folder"));
                table.setPrefHeight(220);
                pane.getChildren().add(table);
            }),
            new Category("Viewing / editing a file", "openingFile.fxml", null)
    };

    @FXML
    public void initialize() {
        listViewCategories.setCellFactory(param -> new CategoryCell(content));
        listViewCategories.getItems().setAll(categories);
        listViewCategories.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                Category item = listViewCategories.getSelectionModel().getSelectedItem();
                if (item != null) content.setContent(item.pane);
            }
        });
        listViewResults.setCellFactory(param -> new ResultCell(content));
        root.getItems().remove(2);
        content.contentProperty().addListener(observable -> {
            root.getItems().set(1, content);
            search.setText(null);
        });
        search.textProperty().addListener((observable, oldValue, newValue) -> search(newValue));
    }

    private void search(String query) {
        if (query == null || query.isEmpty()) {
            root.getItems().set(1, content);
            return;
        } else root.getItems().set(1, listViewResults);
        listViewResults.setMaxHeight(Double.MAX_VALUE);
        List<Pair<Category, TextFlow>> items = listViewResults.getItems();
        items.clear();
        for (Category category : categories) {
            for (TextFlow r : category.search(query)) items.add(new Pair<>(category, r));
        }
    }

    private static class CategoryCell extends ListCell<Category> {

        private final EventHandler<MouseEvent> handler;

        private CategoryCell(ScrollPane content) {
            handler = event -> content.setContent(getItem().pane);
        }

        @Override
        protected void updateItem(Category item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setText(null);
                setOnMouseClicked(null);
            } else {
                setText(item.title);
                setOnMouseClicked(handler);
            }
        }

    }

    private static class ResultCell extends ListCell<Pair<Category, TextFlow>> {

        private final VBox vbox;
        private final Label title, nullLabel;
        private final EventHandler<MouseEvent> handler;

        private ResultCell(ScrollPane content) {
            handler = event -> {
                content.setContent(null);
                content.setContent(getItem().getKey().pane);
            };
            title = new Label();
            title.setWrapText(true);
            title.setFont(Font.font(null, FontWeight.BOLD, 14d));
            vbox = new VBox(5d);
            vbox.setPadding(new Insets(10d));
            vbox.getChildren().setAll(title, nullLabel = new Label());
            setGraphic(vbox);
        }

        @Override
        protected void updateItem(Pair<Category, TextFlow> item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                title.setText(null);
                vbox.getChildren().set(1, nullLabel);
                setOnMouseClicked(null);
            } else {
                title.setText(item.getKey().title);
                vbox.getChildren().set(1, item.getValue());
                setOnMouseClicked(handler);
            }
        }

    }
}
