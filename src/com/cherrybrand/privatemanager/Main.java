package com.cherrybrand.privatemanager;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main extends Application {

    static final List<Image> ICONS = new ArrayList<>(4);

    static {
        for (int i = 128; i >= 16; i /= 2) ICONS.add(new Image(Main.class.getResourceAsStream("/icon.png"), 128, 0, true, true));
    }

    private Stage stage;
    private Manager manager;
    private Manager.Folder root;
    Manager.Folder current;

    @FXML
    Label labelUser;
    @FXML
    private Button buttonMore, buttonChangeName, buttonChangePassword, buttonDeleteUser, buttonUp;
    private final ContextMenu buttonMoreMenu = new ContextMenu();
    @FXML
    private ToolBar toolbar;
    @FXML
    private TextField address;
    private String addressOld;
    private Manager.Folder addressFolder;
    @FXML
    private ListView<Manager.File> content;
    private MultipleSelectionModel<Manager.File> contentSelection;

    private final List<Manager.File> clipboardFiles = new ArrayList<>();
    private Manager.Folder clipboardFrom;
    private boolean clipboardCut;

    private final ContextMenu fileMenu = new ContextMenu(), folderMenu = new ContextMenu(), multipleMenu = new ContextMenu(), listMenu = new ContextMenu();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        // Init stage
        this.stage = stage;
        stage.getIcons().setAll(ICONS);
        stage.setTitle("Private Manager");
        stage.setOnCloseRequest(event -> {
            if (!manager.cleanup(true)) event.consume();
            else {
                logout();
                manager.saveWindowLocation(this.stage);
            }
        });
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("Main.fxml"));
        loader.setController(this);
        Scene scene = new Scene(loader.load());
        stage.setScene(scene);
        // Init more context menu
        buttonMore.setContextMenu(buttonMoreMenu);
        MenuItem item;
        item = new MenuItem("Current status");
        item.setId("status");
        item.setGraphic(new ImageView(new Image(Main.class.getResourceAsStream("img/status.png"), 18, 0, true, true)));
        item.setOnAction(moreHandler);
        buttonMoreMenu.getItems().add(item);
        item = new MenuItem("Full cleanup");
        item.setId("cleanup");
        item.setGraphic(new ImageView(new Image(Main.class.getResourceAsStream("img/cleanup.png"), 18, 0, true, true)));
        item.setOnAction(moreHandler);
        buttonMoreMenu.getItems().add(item);
        item = new MenuItem("Encrypt with advanced file selection");
        item.setId("add");
        item.setGraphic(new ImageView(new Image(Main.class.getResourceAsStream("img/input.png"), 18, 0, true, true)));
        item.setOnAction(moreHandler);
        buttonMoreMenu.getItems().add(item);
        item = new MenuItem("Decrypt with advanced file selection");
        item.setId("remove");
        item.setGraphic(new ImageView(new Image(Main.class.getResourceAsStream("img/output.png"), 18, 0, true, true)));
        item.setOnAction(moreHandler);
        buttonMoreMenu.getItems().add(item);
        item = new MenuItem("Help");
        item.setId("help");
        item.setGraphic(new ImageView(new Image(Main.class.getResourceAsStream("img/help.png"), 18, 0, true, true)));
        item.setOnAction(moreHandler);
        buttonMoreMenu.getItems().add(item);
        item = new MenuItem("About");
        item.setId("about");
        item.setGraphic(new ImageView(new Image(Main.class.getResourceAsStream("img/about.png"), 18, 0, true, true)));
        item.setOnAction(moreHandler);
        buttonMoreMenu.getItems().add(item);
        // Load icons
        iconFolder = new Image(Main.class.getResourceAsStream("img/folder.png"), 18, 0, true, true);
        iconFile = new Image(Main.class.getResourceAsStream("img/file.png"), 18, 0, true, true);
        // Init list view
        contentSelection = content.getSelectionModel();
        contentSelection.setSelectionMode(SelectionMode.MULTIPLE);
        content.setCellFactory(param -> new ItemCell());
        // Init drag and drop stuff
        content.setOnDragExited(event -> {
            content.setStyle("");
            event.consume();
        });
        // Init context menus
        // Manage commands
        addMenuItem("open", "View (read only mode)", fileMenu);
        addMenuItem("edit", "Edit (edit mode)", fileMenu);
        addMenuItem("open", "Open folder", folderMenu);
        addMenuItem("decrypt", "Move out of private folder", fileMenu, folderMenu, multipleMenu);
        // Edit commands
        addSeparatorMenuItem(fileMenu, folderMenu);
        addMenuItem("copy", "Copy", fileMenu, folderMenu, multipleMenu);
        addMenuItem("cut", "Cut", fileMenu, folderMenu, multipleMenu);
        addMenuItem("delete", "Delete", fileMenu, folderMenu, multipleMenu);
        addMenuItem("rename", "Rename", fileMenu, folderMenu);
        // List commands
        addSeparatorMenuItem(fileMenu, folderMenu, multipleMenu);
        addMenuItem("paste", "Paste", fileMenu, folderMenu, listMenu, multipleMenu);
        pasteButtons = new MenuItem[4];
        pasteButtons[0] = fileMenu.getItems().filtered(menuItem -> "paste".equals(menuItem.getId())).get(0);
        pasteButtons[1] = folderMenu.getItems().filtered(menuItem -> "paste".equals(menuItem.getId())).get(0);
        pasteButtons[2] = listMenu.getItems().filtered(menuItem -> "paste".equals(menuItem.getId())).get(0);
        pasteButtons[3] = multipleMenu.getItems().filtered(menuItem -> "paste".equals(menuItem.getId())).get(0);
        addMenu("New", Arrays.asList(new Pair<>("newFile", "New File"), new Pair<>("newFolder", "New Folder")), fileMenu, folderMenu, listMenu, multipleMenu);
        // Set context menu according to selection
        contentSelection.getSelectedItems().addListener((ListChangeListener<? super Manager.File>) change -> {
            if (contentSelection.getSelectedItems().size() > 1) content.setContextMenu(multipleMenu);
            else if (contentSelection.getSelectedItems().size() == 1) content.setContextMenu(contentSelection.getSelectedItem() instanceof Manager.Folder ? folderMenu : fileMenu);
            else content.setContextMenu(listMenu);
        });
        // Keyboard shortcuts
        content.setOnKeyPressed(event -> {
            if (root == null) return; // logged out, don't do anything
            switch (event.getCode()) {
                case N:
                    if (event.isControlDown()) create();
                    break;
                case ESCAPE:
                    contentSelection.clearSelection();
                    break;
                case ENTER:
                    if (contentSelection.getSelectedItem() != null) performAction("open");
                    break;
                case BACK_SPACE:
                    if (current != root) up();
                    break;
                case HOME:
                    home();
                    break;
                case F2:
                    if (contentSelection.getSelectedItem() != null) performAction("rename");
                    break;
                case DELETE:
                    if (contentSelection.getSelectedItem() != null) performAction("delete");
                    break;
                case C:
                    if (event.isControlDown() && contentSelection.getSelectedItem() != null) performAction("copy");
                    break;
                case X:
                    if (event.isControlDown() && contentSelection.getSelectedItem() != null) performAction("cut");
                    break;
                case V:
                    if (event.isControlDown()) performAction("paste");
                    break;
            }
        });
        // Address field
        address.setOnAction(event -> {
            // Only use normal slashes
            String text = address.getText().trim().replace('\\', '/');
            // Check for empty path
            if (text.isEmpty()) {
                home();
                return;
            }
            // Navigate to the path the user typed
            Manager.Folder folder = root;
            boolean ok = true;
            int prev;
            // Remove first slash if text starts with one
            if (text.charAt(0) == '/') prev = 1;
            else prev = 0;
            for (int i = prev; i < text.length(); i++) {
                if (text.charAt(i) == '/') {
                    Manager.File f = folder.find(text.substring(prev, i));
                    prev = i + 1;
                    if (!(f instanceof Manager.Folder)) {
                        ok = false;
                        break;
                    }
                    folder = (Manager.Folder) f;
                }
            }
            // If the path was invalid, show an error
            if (!ok) {
                Dialogs.message(stage, "Invalid path", "The path you entered is invalid. Please re-check the path you entered and try again.");
                // Change the text back
                updateAddressBar();
                return;
            }
            // If the text didn't end with a slash, check if last child is folder or file
            if (prev != text.length()) {
                Manager.File f = folder.find(text.substring(prev));
                if (f == null) {
                    // Make sure the file / folder exists
                    Dialogs.message(stage, "Invalid path", "The path you entered is invalid. Please re-check the path you entered and try again.");
                    updateAddressBar();
                    return;
                }
                // If it's a folder navigate to it
                if (f instanceof Manager.Folder) folder = (Manager.Folder) f;
                else {
                    // If it's a file open it (in view mode) and reset the text
                    manager.openFile(f, false);
                    updateAddressBar();
                    return;
                }
            }
            displayFolder(folder);
        });
        address.focusedProperty().addListener(observable -> {
            if (!address.isFocused()) updateAddressBar();
        });
        // Initialize user data manager
        manager = new Manager(this, stage);
        manager.init();
        // Show stage
        manager.loadWindowLocation(stage);
        stage.setMinWidth(500);
        stage.setMinHeight(400);
        stage.show();
        Rectangle2D screen = Screen.getPrimary().getVisualBounds();
        double x = stage.getX(), y = stage.getY(), width = stage.getWidth(), height = stage.getHeight();
        if (x < -(width / 2)) stage.setX(-(width / 2));
        if (x > screen.getWidth() - width / 2) stage.setX(screen.getWidth() - width / 2);
        if (y < -(height / 2)) stage.setY(-(height / 2));
        if (y > screen.getHeight() - height / 2) stage.setY(screen.getHeight() - height / 2);
        // Set crash / error screen
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            if (Platform.isFxApplicationThread()) criticalError(throwable.getMessage(), throwable);
            else Platform.runLater(() -> criticalError(throwable.getMessage(), throwable));
        });
        // Show login dialog
        changeUser();
    }

    // Util method used above
    private void addMenuItem(String name, String text, ContextMenu... menus) {
        for (ContextMenu menu : menus) {
            MenuItem item = new MenuItem(text);
            item.setId(name);
            item.setOnAction(contextMenuHandler);
            menu.getItems().add(item);
        }
    }

    // Same
    @SuppressWarnings("SameParameterValue")
    private void addMenu(String text, Collection<Pair<String, String>> items, ContextMenu... menus) {
        for (ContextMenu contextMenu : menus) {
            Menu menu = new Menu(text);
            for (Pair<String, String> i : items) {
                MenuItem item = new MenuItem(i.getValue());
                item.setId(i.getKey());
                item.setOnAction(contextMenuHandler);
                menu.getItems().add(item);
            }
            contextMenu.getItems().add(menu);
        }
    }

    // Same
    private void addSeparatorMenuItem(ContextMenu... menus) {
        for (ContextMenu menu : menus) menu.getItems().add(new SeparatorMenuItem());
    }

    // An event handler for context menus, simply call performAction with a string
    private final EventHandler<ActionEvent> contextMenuHandler = event -> performAction(((MenuItem) event.getTarget()).getId());

    private void performAction(String action) {
        MultipleSelectionModel<Manager.File> selected = content.getSelectionModel();
        // Perform required action
        switch (action) {
            case "open":
                Manager.File f = selected.getSelectedItem();
                if (f instanceof Manager.Folder) displayFolder((Manager.Folder) selected.getSelectedItem());
                else manager.openFile(selected.getSelectedItem(), false);
                break;
            case "edit":
                manager.openFile(selected.getSelectedItem(), true);
                break;
            case "decrypt":
                removeFiles();
                break;
            case "copy":
                // Set clipboard to selected items
                clipboardFiles.clear();
                clipboardFiles.addAll(selected.getSelectedItems());
                clipboardFrom = current;
                clipboardCut = false; // We are copying files
                enableDisablePaste();
                break;
            case "cut":
                // Set clipboard to selected items
                clipboardFiles.clear();
                clipboardFiles.addAll(selected.getSelectedItems());
                clipboardFrom = current;
                clipboardCut = true; // We are cutting / moving files
                enableDisablePaste();
                break;
            case "delete":
                Dialogs.deleteFiles(stage, manager, selected.getSelectedItems());
                clipboardFiles.removeIf(file -> file.deleted);
                enableDisablePaste();
                break;
            case "rename":
                Dialogs.rename(stage, manager, selected.getSelectedItem());
                break;
            case "paste":
                if (!clipboardFiles.isEmpty()) manager.moveOrCopyFiles(clipboardFiles, clipboardCut, clipboardFrom, current);
                enableDisablePaste();
                break;
            case "newFile":
                Dialogs.newFileOrFolder(stage, manager, true);
                break;
            case "newFolder":
                Dialogs.newFileOrFolder(stage, manager, false);
                break;
        }
    }

    private MenuItem[] pasteButtons;

    private void enableDisablePaste() {
        if (clipboardFiles.isEmpty()) for (MenuItem item : pasteButtons) item.setDisable(true);
        else for (MenuItem item : pasteButtons) item.setDisable(false);
    }

    // Called on successful login
    void login() {
        // Add the context menu to the list
        content.setContextMenu(listMenu);
        // Update the label specifying the current user
        labelUser.setText(manager.currentUser.name);
        // Enable all the buttons
        enableDisableButtons(false);
        // Set the root folder & load it
        root = manager.currentUser.root;
        home();
    }

    void logout() {
        // Remove context menu
        content.setContextMenu(null);
        // Update label
        labelUser.setText("<none>");
        // Disable buttons
        enableDisableButtons(true);
        buttonUp.setDisable(true);
        // Clear the files list
        content.getItems().clear();
        manager.cleanupSilent();
        // Logout from manager if logged in (this method is called on startup when not logged in to set the label & disable the buttons)
        if (manager.currentUser != null) manager.currentUser.logout();
        manager.currentUser = null;
        // Remove all references to logged user, including clipboard
        current = null;
        root = null;
        clipboardFrom = null;
        clipboardFiles.clear();
        // Disable paste buttons
        enableDisablePaste();
    }

    private void enableDisableButtons(boolean disable) {
        buttonChangeName.setDisable(disable);
        buttonChangePassword.setDisable(disable);
        buttonDeleteUser.setDisable(disable);
        address.setDisable(disable);
        for (Node node : toolbar.getItems()) if (node != buttonMore) node.setDisable(disable);
    }

    // Used to be able to refresh without scrolling up
    private boolean refreshing = false;

    void refreshDisplay() {
        refreshing = true;
        displayFolder(current);
    }

    // This is set to false when displayFolder may be called from a non-FX thread
    private boolean enableDisplay = true;

    private void displayFolder(Manager.Folder folder) {
        if (enableDisplay) {
            // Update display
            this.current = folder;
            // Can't go up if currently in root folder
            buttonUp.setDisable(folder == root);
            // Sort the display the files
            folder.sort();
            content.getItems().setAll(folder.files);
            // Scroll up if needed & set address bar
            if (refreshing) refreshing = false;
            else content.scrollTo(0);
            updateAddressBar();
        }
    }

    private void updateAddressBar() {
        // If no changes were done, don't process address again
        if (addressFolder == current) {
            address.setText(addressOld);
            return;
        }
        // Process folder hierarchy and get address
        addressFolder = current;
        Manager.Folder folder = current;
        List<String> hierarchy = new ArrayList<>();
        while (folder != root) {
            hierarchy.add(folder.name);
            folder = folder.parent;
        }
        StringBuilder builder = new StringBuilder();
        for (int i = hierarchy.size() - 1; i >= 0; i--) builder.append(hierarchy.get(i)).append('/');
        addressOld = builder.toString();
        address.setText(addressOld);
    }

    // All of these methods (public) are called when a button is pressed

    // User management

    @FXML
    public void changeUser() {
        if (manager.cleanup(true)) {
            logout();
            Dialogs.users(stage, manager);
        }
    }

    @FXML
    public void changeUsername() {
        Dialogs.changeUsername(stage, manager);
    }

    @FXML
    public void changePassword() {
        Dialogs.changePassword(stage, manager);
    }

    @FXML
    public void deleteAccount() {
        Dialogs.deleteAccount(stage, manager);
    }

    // Toolbar

    @FXML
    public void home() {
        displayFolder(root);
    }

    @FXML
    public void addFiles() {
        // Choose input directory
        Dialogs.message(stage, "Choose root", "Choose from where to add files. You will be able to select each individual subfolder or file.");
        File folder = chooseAddDirectory(stage);
        if (folder == null) return;
        // Ask user which files to add
        Path dest = folder.toPath();
        try {
            TreeDialog.TreeNodePath root = new TreeDialog.TreeNodePath();
            try (Stream<Path> stream = Files.list(dest)) {
                stream.forEach(path -> root.add(new TreeDialog.TreeNodePath(path)));
            }
            List<TreeDialog.TreeNodePath> selected = TreeDialog.show(stage, root, true).getChildren();
            if (selected.isEmpty()) return;
            // Ask whether to copy or move the files
            int copyOrMove = Dialogs.copyOrMove(stage);
            if (copyOrMove == 0) return;
            // Encrypt the files
            new AsyncAddFiles<TreeDialog.TreeNodePath>(copyOrMove, selected) {
                @Override
                void addFile(TreeDialog.TreeNodePath file) throws Dialogs.OperationCancelled {
                    addFile(current, file.getPath());
                    manager.currentUser.saveHierarchy();
                }
            }.start(false);
        } catch (Exception e) {
            if (e instanceof SecurityException) Dialogs.securityException(stage, false, dest.toString(), (SecurityException) e);
            else error("Unable to access directory: " + dest, e);
        }
    }

    abstract class AsyncAdd<T> extends Dialogs.AsyncTask<Void> {

        final int copyOrMove;
        final Main main;
        final Manager manager;
        private final Stage stage;
        private final List<T> files;
        // Remembers all conflicts that have already occurred to perform same action if user checks "Apply to all"
        private final Conflict.OccurredConflicts conflicts = new Conflict.OccurredConflicts();

        String exceptionText, exceptionFile;
        boolean file;
        Exception exception;

        AsyncAdd(int copyOrMove, List<T> files) {
            super(Main.this.stage, "Adding files...", "Please wait, files are being added to your private folder...", true);
            this.copyOrMove = copyOrMove;
            this.main = Main.this;
            this.manager = Main.this.manager;
            this.files = files;
            this.stage = Main.this.stage;
        }

        @Override
        void beforeRun() {
            enableDisplay = false;
        }

        @Override
        Void run() {
            try {
                for (T t : files) {
                    if (isCancelled()) break;
                    addFile(t);
                }
            } catch (Dialogs.OperationCancelled e) {
                cancel();
            }
            return null;
        }

        @SuppressWarnings("Duplicates")
        @Override
        void afterRun(Void result) {
            enableDisplay = true;
            refreshDisplay();
            if (exception != null) {
                if (exception instanceof SecurityException) Dialogs.securityException(stage, file, exceptionFile, (SecurityException) exception);
                else error(exceptionText, exception);
            } else if (!isCancelled()) Dialogs.message(stage, "Success", "Files were moved / copied successfully.");
        }

        abstract void addFile(T file) throws Dialogs.OperationCancelled;

        @SuppressWarnings("Duplicates")
        Conflict.ConflictResult onConflict(String oldName, String newName, boolean oldDir, boolean newDir) {
            // Check what kind of conflict we have
            Conflict.ConflictType type;
            if (oldDir && newDir) type = Conflict.ConflictType.FOLDER_FOLDER;
            else if (!oldDir && !newDir) type = Conflict.ConflictType.FILE_FILE;
            else if (oldDir) type = Conflict.ConflictType.FOLDER_FILE;
            else type = Conflict.ConflictType.FILE_FOLDER;
            // Check if we already had this conflict before and user checked "Apply to all"
            Conflict.ConflictResult result = conflicts.getResult(type);
            // Ask user if not
            if (result == null) {
                result = runOnFxThreadAndWait(() -> Conflict.show(dialog, type, oldName, newName));
                conflicts.onConflict(type, result);
            }
            return result;
        }
    }

    private abstract class AsyncAddFiles<T> extends AsyncAdd<T> {

        AsyncAddFiles(int copyOrMove, List<T> files) {
            super(copyOrMove, files);
        }

        // Move a file into the private folder
        @SuppressWarnings("Duplicates")
        void addFile(Manager.Folder folder, Path file) throws Dialogs.OperationCancelled {
            checkCancelled();
            // Obtain filename and folder
            String filename = file.getFileName().toString();
            Manager.File f = folder.find(filename);
            try {
                // On conflict
                if (f != null) {
                    // Perform action based on result
                    String oldName = filename, newName = Utils.findFilename(filename, s -> folder.find(s) == null);
                    boolean oldDir = Files.isDirectory(file), newDir = f instanceof Manager.Folder;
                    switch (onConflict(oldName, newName, oldDir, newDir).getResult()) {
                        case Conflict.ConflictResult.CANCEL:
                            throw new Dialogs.OperationCancelled();
                        case Conflict.ConflictResult.SKIP:
                            return;
                        case Conflict.ConflictResult.MERGE_OR_REPLACE:
                            // If we have folder - folder conflict, nothing needs to be done
                            if (!(oldDir && newDir)) {
                                // Delete existing file or folder
                                try {
                                    manager.deleteFiles(Collections.singletonList(f));
                                } catch (Exception e) {
                                    exception = e;
                                    this.file = true;
                                    exceptionFile = f.getPrivatePath();
                                    exceptionText = "Unable to delete file or folder: " + exceptionFile;
                                    throw new Dialogs.OperationCancelled();
                                }
                            }
                            break;
                        case Conflict.ConflictResult.RENAME_EXISTING:
                            // Rename existing file / folder
                            manager.rename(f, newName);
                            break;
                        case Conflict.ConflictResult.RENAME_NEW:
                            // Rename new file / folder
                            filename = newName;
                            break;
                    }
                }
                // Perform the encryption
                if (Files.isDirectory(file)) {
                    Manager.Folder newFolder = new Manager.Folder(filename, folder);
                    folder.files.add(newFolder);
                    // Close needs to be called before deleteDirectory
                    try (Stream<Path> list = Files.list(file)) {
                        // Can't use forEach due to OperationCancelled may be thrown inside lambda
                        List<Path> paths = list.collect(Collectors.toList());
                        for (Path path : paths) addFile(newFolder, path);
                    } catch (IOException e) {
                        exception = e;
                        this.file = false;
                        exceptionFile = file.toString();
                        exceptionText = "Unable to read directory: " + exceptionFile;
                        throw new Dialogs.OperationCancelled();
                    }
                    // Delete the old file if needed
                    if (copyOrMove == -1) {
                        try {
                            Utils.deleteDirectory(file);
                        } catch (Exception e) {
                            exception = e;
                            this.file = false;
                            exceptionFile = file.toString();
                            exceptionText = "Unable to delete folder: " + exceptionFile;
                            throw new Dialogs.OperationCancelled();
                        }
                    }
                } else {
                    f = new Manager.File(filename, manager.currentUser, folder);
                    try {
                        manager.encryptFile(f, file);
                        folder.files.add(f);
                    } catch (Exception e) {
                        exception = e;
                        this.file = true;
                        exceptionFile = file + " OR" + f.name;
                        exceptionText = "Unable to encrypt file: " + file + " into " + f.name;
                        throw new Dialogs.OperationCancelled();
                    }
                    // Delete the old file if needed
                    if (copyOrMove == -1) {
                        try {
                            Files.delete(file);
                        } catch (Exception e) {
                            exception = e;
                            this.file = true;
                            exceptionFile = file.toString();
                            exceptionText = "Unable to delete file: " + exceptionFile;
                            throw new Dialogs.OperationCancelled();
                        }
                    }
                }
            } catch (SecurityException e) {
                exception = e;
                this.file = true;
                exceptionFile = file.toString();
                throw new Dialogs.OperationCancelled();
            }
        }
    }

    @FXML
    public void removeFiles() {
        if (contentSelection.isEmpty()) Dialogs.message(stage, "Nothing selected", "No files selected. Please select some files and try again.");
        else {
            // Save any in-edit files
            if (!manager.checkEditing()) return;
            // Ask user whether to copy or move the files
            int copyOrMove = Dialogs.copyOrMove(stage);
            if (copyOrMove == 0) return;
            // Choose output directory
            File folder = chooseRemoveDirectory(stage);
            if (folder == null) return;
            // Perform the recursive file decryption
            Path dest = folder.toPath();
            new AsyncRemove<Manager.File>(copyOrMove, contentSelection.getSelectedItems()) {
                @Override
                void removeFile(Manager.File file) throws Dialogs.OperationCancelled {
                    if (removeFile(dest, file)) // File is deleted here
                        // Removing from parent is always done by caller method
                        // To avoid ConcurrentModificationException in iterations
                        if (copyOrMove == -1) file.parent.files.remove(file);
                    manager.currentUser.saveHierarchy();
                }

                // Move a file outside from the private folder
                @SuppressWarnings("Duplicates")
                private boolean removeFile(Path folder, Manager.File f) throws Dialogs.OperationCancelled {
                    checkCancelled();
                    try {
                        Files.createDirectories(folder);
                    } catch (Exception e) {
                        exception = e;
                        this.file = false;
                        this.exceptionFile = folder.toString();
                        this.exceptionText = "Unable to create folder: " + exceptionFile;
                        throw new Dialogs.OperationCancelled();
                    }
                    Path dest = folder.resolve(f.name);
                    try {
                        if (Files.exists(dest)) {
                            // Check what kind of conflict we have
                            String oldName = f.name, newName = Utils.findFilename(oldName, s -> !Files.exists(folder.resolve(s)));
                            boolean oldDir = f instanceof Manager.Folder, newDir = Files.isDirectory(dest);
                            // Perform action based on result
                            switch (onConflict(oldName, newName, oldDir, newDir).getResult()) {
                                case Conflict.ConflictResult.CANCEL:
                                    throw new Dialogs.OperationCancelled();
                                case Conflict.ConflictResult.SKIP:
                                    return false;
                                case Conflict.ConflictResult.MERGE_OR_REPLACE:
                                    // If we have folder - folder conflict, nothing needs to be done
                                    if (!(oldDir && newDir)) {
                                        // Delete existing file or folder
                                        try {
                                            if (oldDir) Utils.deleteDirectory(dest);
                                            else Files.delete(dest);
                                        } catch (Exception e) {
                                            exception = e;
                                            this.file = true;
                                            exceptionFile = dest.toString();
                                            exceptionText = "Unable to delete file or folder: " + dest;
                                            throw new Dialogs.OperationCancelled();
                                        }
                                    }
                                    break;
                                case Conflict.ConflictResult.RENAME_EXISTING:
                                    // Rename existing file / folder
                                    try {
                                        Files.move(dest, folder.resolve(newName));
                                    } catch (Exception e) {
                                        exception = e;
                                        this.file = true;
                                        exceptionFile = dest + " OR " + newName;
                                        exceptionText = "Unable to rename " + dest + " to " + newName;
                                        throw new Dialogs.OperationCancelled();
                                    }
                                    break;
                                case Conflict.ConflictResult.RENAME_NEW:
                                    // Rename new file / folder
                                    dest = folder.resolve(newName);
                                    break;
                            }
                        }
                    } catch (SecurityException e) {
                        exception = e;
                        this.file = true;
                        exceptionFile = dest.toString();
                        throw new Dialogs.OperationCancelled();
                    }
                    // Perform the decryption
                    if (f instanceof Manager.Folder) {
                        // Remove each file
                        boolean deletedAll = true;
                        for (Iterator<Manager.File> it = ((Manager.Folder) f).iterator(); it.hasNext(); ) {
                            if (removeFile(dest, it.next())) {// Deleting is done here (if copyOrMove == -1)
                                // Remove file from parent (if deleting)
                                if (copyOrMove == -1) it.remove();
                            } else deletedAll = false;
                        }
                        return deletedAll;
                    } else {
                        // Decrypt file
                        try {
                            manager.decryptFile(dest, f);
                        } catch (Exception e) {
                            exception = e;
                            this.file = true;
                            exceptionFile = dest + " OR " + f.getFile();
                            exceptionText = "Unable to decrypt file: " + f.name + " into " + dest;
                            throw new Dialogs.OperationCancelled();
                        }
                        // If moving, delete old file
                        if (copyOrMove == -1 && !manager.deleteFile(f)) {
                            // Note: removal from parent is done by caller method
                            runOnFxThreadAndWait(() -> Dialogs.message(stage, "Error", "Unable to delete file: " + f.getFile()));
                            throw new Dialogs.OperationCancelled();
                        }
                    }
                    return true;
                }
            }.start(false);
        }
    }

    abstract class AsyncRemove<T> extends Dialogs.AsyncTask<Void> {

        private final List<T> files;
        final int copyOrMove;
        final Manager manager;
        final Stage stage;
        // Remembers all conflicts that have already occurred to perform same action if user checks "Apply to all"
        private final Conflict.OccurredConflicts conflicts = new Conflict.OccurredConflicts();

        String exceptionText, exceptionFile;
        boolean file;
        Exception exception;

        AsyncRemove(int copyOrMove, List<T> files) {
            super(Main.this.stage, "Moving / copying files...", "Please wait, files are being moved / copied outside from your private folder...", true);
            this.files = files;
            this.copyOrMove = copyOrMove;
            this.manager = Main.this.manager;
            this.stage = Main.this.stage;
        }

        @Override
        void beforeRun() {
            enableDisplay = false;
        }

        @Override
        Void run() {
            try {
                for (T t : files) {
                    if (isCancelled()) break;
                    removeFile(t);
                }
            } catch (Dialogs.OperationCancelled ignore) {
            }
            return null;
        }

        abstract void removeFile(T file) throws Dialogs.OperationCancelled;

        @SuppressWarnings("Duplicates")
        Conflict.ConflictResult onConflict(String oldName, String newName, boolean oldDir, boolean newDir) {
            // Check what kind of conflict we have
            Conflict.ConflictType type;
            if (oldDir && newDir) type = Conflict.ConflictType.FOLDER_FOLDER;
            else if (!oldDir && !newDir) type = Conflict.ConflictType.FILE_FILE;
            else if (oldDir) type = Conflict.ConflictType.FOLDER_FILE;
            else type = Conflict.ConflictType.FILE_FOLDER;
            // Check if we already had this conflict before and user checked "Apply to all"
            Conflict.ConflictResult result = conflicts.getResult(type);
            // Ask user if not
            if (result == null) {
                result = runOnFxThreadAndWait(() -> Conflict.show(dialog, type, oldName, newName));
                conflicts.onConflict(type, result);
            }
            return result;
        }

        @SuppressWarnings("Duplicates")
        @Override
        void afterRun(Void result) {
            enableDisplay = true;
            refreshDisplay();
            if (exception != null) {
                if (exception instanceof SecurityException) Dialogs.securityException(stage, file, exceptionFile, (SecurityException) exception);
                else error(exceptionText, exception);
            } else if (!isCancelled()) Dialogs.message(stage, "Success", "Files were moved / copied successfully.");
        }
    }

    private File chooseAddDirectory(Stage owner) {
        File folder = chooseDirectory(owner, manager.loadAddDirectory(), "Add files to your private folder...");
        if (folder != null) manager.saveAddDirectory(folder.toString());
        return folder;
    }

    File chooseRemoveDirectory(Stage owner) {
        File folder = chooseDirectory(owner, manager.loadRemoveDirectory(), "Select where to put the selected files...");
        if (folder != null) manager.saveRemoveDirectory(folder.toString());
        return folder;
    }

    private static File chooseDirectory(Stage owner, String initial, String title) {
        DirectoryChooser chooser = new DirectoryChooser();
        if (initial != null) {
            File f = new File(initial);
            try {
                if (f.isDirectory()) chooser.setInitialDirectory(f);
            } catch (SecurityException ignore) {
            }
        }
        chooser.setTitle(title);
        File folder = chooser.showDialog(owner);
        // Cancel if user didn't pick a directory
        if (folder == null) return null;
        boolean dir;
        try {
            dir = folder.isDirectory();
        } catch (SecurityException e) {
            Dialogs.securityException(owner, false, folder.toString(), e);
            return null;
        }
        if (!dir) Dialogs.message(owner, "Invalid folder selected", "Could not access the selected folder, please choose a new one.");
        else return folder;
        return null;
    }

    @FXML
    public void create() { // New folder / file
        Dialogs.create(stage, manager);
    }

    @FXML
    public void apply() {
        manager.applyEdited();
    }

    @FXML
    public void cleanup() {
        manager.cleanup(false);
    }

    @FXML
    public void passwords() {
        PasswordManager.show(stage, manager);
    }

    @FXML
    public void more(MouseEvent event) {
        buttonMoreMenu.show(buttonMore, event.getScreenX(), event.getScreenY());
    }

    private final EventHandler<ActionEvent> moreHandler = event -> {
        switch (((MenuItem) event.getTarget()).getId()) {
            case "status":
                Dialogs.status(stage, manager);
                break;
            case "cleanup":
                Dialogs.fullCleanup(stage, manager);
                break;
            case "add":
                if (root == null) return;
                File folder = chooseAddDirectory(stage);
                if (folder != null) FileSelector.show(stage, this, folder.toPath());
                break;
            case "remove":
                if (root == null) return;
                if (contentSelection.getSelectedItems().isEmpty()) Dialogs.message(stage, "No files selected", "Please select some files / folders before and try again.");
                else FileSelector.show(stage, this, contentSelection.getSelectedItems());
                break;
            case "help":
                Help.show(stage);
                break;
            case "about":
                Dialogs.about(stage, this);
                break;
        }
    };

    // Others

    @FXML
    public void up() {
        displayFolder(current.parent);
    }

    // Called when an exception is caught

    void error(String details, Exception e) {
        e.printStackTrace();
        Stage stage = new Stage();
        stage.getIcons().setAll(ICONS);
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Error");
        Scene scene;
        try {
            scene = new Scene(FXMLLoader.load(Main.class.getResource("error.fxml")));
        } catch (Exception ex) {
            throw new Error(ex);
        }
        stage.setScene(scene);
        ((Label) scene.lookup("#labelDetails")).setText(details);
        ((Button) scene.lookup("#button")).setOnAction(event -> stage.close());
        saveLog(scene, e);
        stage.setScene(scene);
        stage.showAndWait();
    }

    // Called when a thread dies by an uncaught exception

    private void criticalError(String details, Throwable t) {
        t.printStackTrace();
        Stage stage = new Stage();
        stage.getIcons().setAll(ICONS);
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Critical error");
        Scene scene;
        try {
            scene = new Scene(FXMLLoader.load(Main.class.getResource("critical.fxml")));
        } catch (Exception e) {
            throw new Error(e);
        }
        stage.setScene(scene);
        ((Label) scene.lookup("#labelDetails")).setText(details);
        ((Button) scene.lookup("#button")).setOnAction(event -> stage.close());
        saveLog(scene, t);
        stage.setScene(scene);
        try {
            stage.showAndWait();
            Platform.exit();
        } catch (IllegalStateException e) {
            stage.show();
            stage.showingProperty().addListener(observable -> Platform.exit());
        }
    }

    private void saveLog(Scene scene, Throwable t) {
        OutputStream stream = null;
        try {
            Path path = manager.createLog();
            stream = Files.newOutputStream(path);
            t.printStackTrace(new PrintStream(stream));
            stream.flush();
            ((Label) scene.lookup("#labelLocation")).setText(path.toString());
        } catch (Exception e) {
            e.printStackTrace();
            ((Label) scene.lookup("#labelLocation")).setText("unable to create log file");
        } finally {
            try {
                if (stream != null) stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Implementation of onDragDropped
    private void onDragDropped(DragEvent event, Manager.Folder dest) {
        // If files come from outside
        if (event.getGestureSource() == null) {
            List<File> files = new ArrayList<>(event.getDragboard().getFiles());
            for (Iterator<File> it = files.iterator(); it.hasNext(); ) {
                try {
                    if (!it.next().canRead()) it.remove();
                } catch (SecurityException e) {
                    it.remove();
                }
            }
            new AsyncAddFiles<File>(event.getTransferMode() == TransferMode.COPY ? 1 : -1, files) {
                @Override
                void addFile(File file) throws Dialogs.OperationCancelled {
                    addFile(dest, file.toPath());
                    manager.currentUser.saveHierarchy();
                }
            }.start(false);
        } else { // If files come from this app
            manager.moveOrCopyFiles(event.getDragboard().getFiles().stream().map(file -> current.find(file.getName())).collect(Collectors.toList()),
                    event.getTransferMode() == TransferMode.MOVE, current, dest);
        }
    }

    private Image iconFolder, iconFile;

    // A class for the items in the file manager
    private class ItemCell extends ListCell<Manager.File> {

        private final ImageView imageView = new ImageView();

        private ItemCell() {
            setOnDragOver(dragOver);
            setOnDragEntered(dragEntered);
            setOnDragExited(dragExited);
            setOnDragDropped(dragDropped);
            setOnDragDone(dragDone);
        }

        @Override
        protected void updateItem(Manager.File item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
                setOnMouseClicked(itemClickedEmpty);
                setOnDragDetected(null);
            } else {
                setText(item.name);
                imageView.setImage(item instanceof Manager.Folder ? iconFolder : iconFile);
                setGraphic(imageView);
                setOnMouseClicked(itemClicked);
                setOnDragDetected(dragStarted);
            }
        }

        // When item is not empty
        private final EventHandler<MouseEvent> itemClicked = event -> {
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() % 2 == 0) {
                // If a file / folder is double-clicked, open it
                Manager.File f = getItem();
                if (f instanceof Manager.Folder) displayFolder((Manager.Folder) f);
                else manager.openFile(f, false);
            }
        };
        // When item is empty
        private final EventHandler<MouseEvent> itemClickedEmpty = event -> contentSelection.clearSelection();

        // Drag and drop
        private final EventHandler<MouseEvent> dragStarted = event -> {
            // Init drag and drop
            Dragboard dragboard = startDragAndDrop(TransferMode.COPY_OR_MOVE);
            // Put files into clipboard content
            ClipboardContent content = new ClipboardContent();
            content.putFilesByPath(contentSelection.getSelectedItems().stream().map(file -> file.name).collect(Collectors.toList()));
            dragboard.setContent(content);
            event.consume();

        };

        private final EventHandler<DragEvent> dragOver = event -> {
            if (event.getDragboard().hasFiles() && event.getGestureSource() != ItemCell.this && (getItem() instanceof Manager.Folder || isEmpty())) {
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            event.consume();
        };

        private final EventHandler<DragEvent> dragEntered = event -> {
            if (event.getDragboard().hasFiles()) {
                if (event.getGestureSource() != ItemCell.this && getItem() instanceof Manager.Folder) setStyle("-fx-base: LIME;");
                else if (isEmpty()) {
                    content.setStyle("-fx-base: LIME;");
                    return;
                }
                content.setStyle("");
            }
            event.consume();
        };

        private final EventHandler<DragEvent> dragExited = event -> {
            if (!isEmpty()) {
                setStyle("");
            }
            event.consume();
        };

        private final EventHandler<DragEvent> dragDropped = event -> onDragDropped(event, isEmpty() ? current : (Manager.Folder) getItem());

        private final EventHandler<DragEvent> dragDone = event -> {
            // TODO
        };
    }

}
