package com.cherrybrand.privatemanager;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.ini4j.Ini;
import org.ini4j.Profile;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.DosFileAttributeView;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFilePermission;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

// Random utilities as well as all encryption / decryption methods
final class Utils {

    // A secure random for crypto
    private static final SecureRandom secure = new SecureRandom();
    // A normal random for file names
    private static final Random random = new Random();
    // Characters allowed when generating a random filename
    private static final char[] chars;
    // Pattern for filenames / username
    private static final Pattern filename = Pattern.compile("[^\\\\/:*?<>|]{1,64}");
    private static final Pattern username = Pattern.compile("[a-zA-Z0-9-_]*");

    // Populate 'chars'
    static {
        StringBuilder builder = new StringBuilder('z' - 'a' + 'Z' - 'A' + '9' - '0' + 3); // basically 'z'-'a'+1 + 'Z'-'A'+1 + '9'-'0'+1
        for (char c = 'a'; c <= 'z'; c++) builder.append(c); // a-z
        for (char c = 'A'; c <= 'Z'; c++) builder.append(c); // A-Z
        for (char c = '0'; c <= '9'; c++) builder.append(c); // 0-9
        chars = new char[builder.length()];
        builder.getChars(0, chars.length, chars, 0);
    }

    // ***********************************
    //
    // INI helper
    //
    // ***********************************

    static class IniHelper {

        private final Ini ini;
        private final Path path;
        private boolean edited;

        IniHelper(Path path) {
            ini = new Ini();
            this.path = path;
            ini.setFile(path.toFile());
        }

        Section get(String name) {
            Profile.Section s = ini.get(name);
            if (s == null) s = ini.add(name);
            return new Section(s, this);
        }

        Path getPath() {
            return path;
        }

        void load() throws IOException {
            if (ini.getFile().exists()) ini.load();
        }

        void save() throws IOException {
            if (edited) {
                ini.store();
                edited = false;
            }
        }

        static class Section {

            private final Profile.Section section;
            private final IniHelper helper;

            private Section(Profile.Section section, IniHelper ini) {
                this.section = section;
                this.helper = ini;
            }

            String get(String name) {
                return section.get(name);
            }

            void set(String name, String value) {
                String s = section.get(name);
                if (value.equals(s)) return;
                if (s != null) section.remove(name);
                section.put(name, value);
                helper.edited = true;
            }

        }

    }

    // ***********************************
    //
    // String utilities
    //
    // ***********************************

    // Generate a random string using characters from 'chars'
    static String nextString() {
        char[] r = new char[16]; // Length is always 16
        for (int i = 0; i < r.length; ++i) r[i] = chars[random.nextInt(chars.length)];
        return new String(r);
    }

    static int countLines(String string) {
        if (string.isEmpty()) return 0;
        int count = 1;
        for (int i = 0; i < string.length(); ++i) if (string.charAt(i) == '\n') ++count;
        return count;
    }

    /**
     * @return <code>true</code> if filename is NOT valid
     */
    static boolean checkFilename(String filename) {
        return !Utils.filename.matcher(filename).matches();
    }

    /**
     * @return <code>true</code> if username is NOT valid
     */
    static boolean checkUsername(String username) {
        return !Utils.username.matcher(username).matches();
    }

    // Split a filename into name and extension
    static String[] splitFilename(String name) {
        int pos = name.lastIndexOf('.');
        if (pos == -1) return new String[]{name, ""};
        else return new String[]{name.substring(0, pos), name.substring(pos)};
    }

    // ***********************************
    //
    // UI utils
    //
    // ***********************************

    public static class TableItem {

        private final String item1, item2;

        private TableItem(String item1, String item2) {
            this.item1 = item1;
            this.item2 = item2;
        }

        @SuppressWarnings("unused")
        public String getItem1() {
            return item1;
        }


        @SuppressWarnings("unused")
        public String getItem2() {
            return item2;
        }

    }

    @SuppressWarnings("SameParameterValue")
    @SafeVarargs
    static TableView<TableItem> createTable(String column1, String column2, Pair<String, String>... values) {
        TableItem[] items = new TableItem[values.length];
        for (int i = 0; i < values.length; ++i) items[i] = new TableItem(values[i].getKey(), values[i].getValue());
        TableView<TableItem> table = new TableView<>(FXCollections.observableArrayList(items));
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.widthProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                TableHeaderRow header = ((TableHeaderRow) table.lookup("TableHeaderRow"));
                header.reorderingProperty().addListener((observable1) -> header.setReordering(false));
                // Can't use lambda because we need to use 'this'
                table.widthProperty().removeListener(this);
            }
        });
        TableColumn<TableItem, String> column;
        column = new TableColumn<>(column1);
        column.setMinWidth(100);
        column.setSortable(false);
        column.setCellValueFactory(new PropertyValueFactory<>("item1"));
        table.getColumns().add(column);
        column = new TableColumn<>(column2);
        column.setMinWidth(100);
        column.setSortable(false);
        column.setCellValueFactory(new PropertyValueFactory<>("item2"));
        table.getColumns().add(column);
        return table;
    }

    // ***********************************
    //
    // Backup utils
    //
    // ***********************************

    interface SaveAction {
        void save(DataOutputStream out) throws IOException;
    }

    interface LoadAction {
        void load(DataInputStream in) throws IOException;
    }

    static void saveFile(Path file, SecretKey key, SaveAction saveAction, Main main, String fileDescription) {
        // Get the paths for the file and it's corresponding backup file
        Path backup = file.resolveSibling(file.getFileName().toString().concat(".backup"));
        try (OutputStream os = Files.newOutputStream(file)) {
            // Perform the save operation
            if (key != null) {
                ByteArrayOutputStream byteOs = new ByteArrayOutputStream();
                saveAction.save(new DataOutputStream(byteOs));
                os.write(encryptData(key, byteOs.toByteArray()));
            } else saveAction.save(new DataOutputStream(os));
            // If save succeeded, save as a backup file too
            // If the next save fails, this file will be used to load the data as it is not changed if the save fails
            Files.copy(file, backup, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            main.error("Unable to save " + fileDescription, e);
        }
    }

    static void loadFile(Path file, SecretKey key, LoadAction loadAction, Main main, Stage owner, String fileDescription) {
        // Get the paths for the file and it's corresponding backup file
        Path backup = file.resolveSibling(file.getFileName().toString().concat(".backup"));
        // Attempt to load the file
        Object att1 = loadFileAttempt(file, key, loadAction);
        if (att1 != null) { // File does not exist or is corrupted
            boolean error = att1 instanceof Exception;
            // If file was corrupted, show a message
            if (error) main.error("Unable to load " + fileDescription + " (corrupted), attempting to load backup...", (Exception) att1);
            // Attempt to load the backup file
            Object att2 = loadFileAttempt(backup, key, loadAction);
            if (error) { // If the original file was corrupted
                if (att2 == null) Dialogs.message(owner, "Success", "Successfully loaded backup file."); // And if backup was loaded
                else if (att2 instanceof Exception) main.error("Unable to load backup file (corrupted)", (Exception) att2); // And if backup was corrupted
                else Dialogs.message(owner, "Error", "Backup file does not exist"); // And if backup didn't exist
            } else { // If the original file didn't exist
                if (att2 == null) Dialogs.message(owner, "Backup loaded", fileDescription + " did not exist, but a backup file was loaded."); // And if backup was loaded
                else if (att2 instanceof Exception) Dialogs.message(owner, "Error", fileDescription +
                        " did not exist and its backup file was corrupted."); // And if backup was corrupted
                // No need to show a message if neither file existed; the file was probably never saved before
            }
        } // else    File loaded successfully
    }

    private static Object loadFileAttempt(Path file, SecretKey key, LoadAction loadAction) {
        if (!Files.exists(file)) return "fnf"; // file not found
        try (InputStream is = Files.newInputStream(file)) {
            // Process the loading
            if (key != null) loadAction.load(new DataInputStream(new ByteArrayInputStream(decryptData(key, readAllBytes(is)))));
            else loadAction.load(new DataInputStream(is));
        } catch (Exception e) {
            return e;
        }
        return null;
    }

    // ***********************************
    //
    // File utilities
    //
    // ***********************************

    private static byte[] readAllBytes(InputStream is) throws IOException {
        byte[] b = new byte[Math.max(1024, is.available())];
        int len, total = 0;
        while ((len = is.read(b, total, b.length - total)) != -1) {
            total += len;
            if (b.length == total) b = Arrays.copyOf(b, b.length * 2);
        }
        return Arrays.copyOf(b, total);
    }

    static void setReadOnly(boolean readOnly, Path path) throws IOException {
        DosFileAttributeView view = Files.getFileAttributeView(path, DosFileAttributeView.class);
        if (view != null) view.setReadOnly(readOnly);
        else {
            PosixFileAttributeView posix = Files.getFileAttributeView(path, PosixFileAttributeView.class);
            Set<PosixFilePermission> permissions = posix.readAttributes().permissions();
            if (readOnly) {
                permissions.remove(PosixFilePermission.OWNER_WRITE);
                permissions.remove(PosixFilePermission.GROUP_WRITE);
                permissions.remove(PosixFilePermission.OTHERS_WRITE);
            } else {
                permissions.add(PosixFilePermission.OWNER_WRITE);
                permissions.add(PosixFilePermission.GROUP_WRITE);
                permissions.add(PosixFilePermission.OTHERS_WRITE);
            }
            posix.setPermissions(permissions);
        }
    }

    static void deleteDirectory(Path path) throws Exception {
        if (!Files.exists(path)) return;
        // Delete each file and sub-directory in the specified folder
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                super.visitFile(file, attrs);
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                super.postVisitDirectory(dir, exc);
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    static String findFilename(String name, Predicate<String> nameAvailable) {
        if (nameAvailable.test(name)) return name;
        String[] nameSplit = Utils.splitFilename(name);
        int count = 0;
        String newName;
        do {
            count++;
            newName = nameSplit[0] + " (" + count + ")" + nameSplit[1];
        } while (!nameAvailable.test(newName));
        return newName;
    }

    // ***********************************
    //
    // Cryptography utilities
    //
    // ***********************************

    static final int MAX_KEY_SIZE;

    static {
        try {
            int maxSize = Cipher.getMaxAllowedKeyLength("AES/CBC/PKCS5Padding");
            if (maxSize > 256) maxSize = 256;
            MAX_KEY_SIZE = maxSize;
        } catch (NoSuchAlgorithmException e) {
            throw new Error(e);
        }
    }

    // Generate a new secure random 16-byte salt
    static byte[] salt() {
        byte[] salt = new byte[16];
        secure.nextBytes(salt);
        return salt;
    }

    // Encrypt a secret key with a password
    static byte[] encryptKey(SecretKey decryptedKey, String password) throws Exception {
        // Generate a salt for the password-based key
        byte[] salt = new byte[16];
        secure.nextBytes(salt);
        // Obtain the password key
        PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 20000, MAX_KEY_SIZE);
        SecretKey key = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256").generateSecret(keySpec);
        keySpec.clearPassword();
        // Initialize the cipher
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] iv = new byte[cipher.getBlockSize()];
        secure.nextBytes(iv);
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key.getEncoded(), "AES"), new IvParameterSpec(iv));
        // Encrypt the key
        byte[] encryptedKey = cipher.doFinal(decryptedKey.getEncoded());
        // Obtain the final encrypted key & return
        byte[] data = new byte[salt.length + iv.length + encryptedKey.length];
        System.arraycopy(salt, 0, data, 0, salt.length);
        System.arraycopy(iv, 0, data, salt.length, iv.length);
        System.arraycopy(encryptedKey, 0, data, salt.length + iv.length, encryptedKey.length);
        return data;
    }

    // Decrypt a secret key with a password
    static SecretKey decryptKey(byte[] encryptedKey, String password) throws Exception {
        SecretKey key = attemptDecryptKey(encryptedKey, password, MAX_KEY_SIZE);
        if (key == null) return attemptDecryptKey(encryptedKey, password, 128);
        else return key;
    }

    private static SecretKey attemptDecryptKey(byte[] encryptedKey, String password, int keySize) throws Exception {
        // Obtain the salt
        byte[] salt = new byte[16];
        System.arraycopy(encryptedKey, 0, salt, 0, salt.length);
        // Obtain the password key
        PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 20000, keySize);
        SecretKey key = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256").generateSecret(keySpec);
        keySpec.clearPassword();
        // Initialize the cipher
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getEncoded(), "AES"), new IvParameterSpec(encryptedKey, salt.length, cipher.getBlockSize()));
        // Decrypt the key and return
        byte[] result;
        try {
            result = cipher.doFinal(encryptedKey, salt.length + cipher.getBlockSize(), encryptedKey.length - salt.length - cipher.getBlockSize());
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            if (keySize > 128) return null;
            else throw e;
        }
        return new SecretKeySpec(result, "AES");
    }

    // Encrypt a byte array
    private static byte[] encryptData(SecretKey key, byte[] data) throws Exception {
        // Generate an IV
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] iv = new byte[cipher.getBlockSize()];
        secure.nextBytes(iv);
        // Init the cipher
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        // Encrypt the data
        byte[] encrypted = cipher.doFinal(data);
        // Return the IV and encrypted data
        byte[] out = new byte[iv.length + encrypted.length];
        System.arraycopy(iv, 0, out, 0, iv.length);
        System.arraycopy(encrypted, 0, out, iv.length, encrypted.length);
        return out;
    }

    // Decrypt a byte array
    private static byte[] decryptData(SecretKey key, byte[] data) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        // Init cipher & obtain IV from the encrypted data
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(data, 0, cipher.getBlockSize()));
        // Decrypt data & return
        return cipher.doFinal(data, cipher.getBlockSize(), data.length - cipher.getBlockSize());
    }

    private static final int BUFFER_SIZE = 4096; // 4 Kilobytes

    // Encrypt a file (src) into dst
    static void encryptFile(SecretKey key, Path src, Path dest) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        // Generate an IV
        byte[] iv = new byte[cipher.getBlockSize()];
        secure.nextBytes(iv);
        // Init cipher
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        byte[] buffer = new byte[BUFFER_SIZE];
        try (InputStream in = Files.newInputStream(src); OutputStream out = Files.newOutputStream(dest)) {
            int read;
            // Write the IV and then the encrypted file
            out.write(iv);
            while ((read = in.read(buffer)) != -1) out.write(cipher.update(buffer, 0, read));
            out.write(cipher.doFinal());
        }
    }

    // True if there is an error
    static boolean encryptAsync(SecretKey key, Path src, Path dest, Stage stage, Main main) {
        return new Dialogs.AsyncTask<Exception>(stage, "Saving file...", "Please wait, saving file...", false) {
            @Override
            Exception run() {
                try {
                    encryptFile(key, src, dest);
                } catch (Exception e) {
                    return e;
                }
                return null;
            }

            @Override
            void afterRun(Exception result) {
                if (result != null) main.error("Error while encrypting file: " + src.getFileName(), result);
            }
        }.start(true) != null;
    }

    // Decrypt a file (src) into dst
    static void decryptFile(SecretKey key, Path src, Path dest) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] iv = new byte[cipher.getBlockSize()];
        try (InputStream in = Files.newInputStream(src); OutputStream out = Files.newOutputStream(dest)) {
            // Read the IV
            if (in.read(iv) != iv.length) throw new IOException("Unable to read initialization vector from file: " + src);
            // Init the cipher
            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
            byte[] buffer = new byte[BUFFER_SIZE];
            int read;
            // Decrypt the file
            while ((read = in.read(buffer)) != -1) out.write(cipher.update(buffer, 0, read));
            out.write(cipher.doFinal());
        }
    }

    static boolean decryptAsync(SecretKey key, Path src, Path dest, Stage stage, Main main) {
        return new Dialogs.AsyncTask<Exception>(stage, "Opening file...", "Please wait, opening file...", false) {
            @Override
            Exception run() {
                try {
                    decryptFile(key, src, dest);
                } catch (Exception e) {
                    return e;
                }
                return null;
            }

            @Override
            void afterRun(Exception result) {
                if (result != null) main.error("Error while decrypting file: " + src.getFileName(), result);
            }
        }.start(true) != null;
    }

}
