package com.cherrybrand.privatemanager;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiPredicate;
import java.util.function.Supplier;

final class Dialogs {

    // User account management

    static void users(Stage owner, Manager manager) {
        Stage stage = stage("Open folder", owner);
        Scene scene = scene("dialog/users.fxml", stage);
        scene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ESCAPE) stage.close();
        });
        if (manager.users.isEmpty()) {
            ((VBox) scene.lookup("#vBox")).getChildren().remove(scene.lookup("#users"));
            scene.lookup("#buttonLogin").setDisable(true);
        } else {
            ((VBox) scene.lookup("#vBox")).getChildren().remove(scene.lookup("#label"));
            //noinspection unchecked
            ListView<String> list = (ListView) scene.lookup("#users");
            Button login = (Button) scene.lookup("#buttonLogin");
            login.setOnAction(event -> {
                if (list.getSelectionModel().getSelectedIndex() != -1)
                    login(list.getSelectionModel().getSelectedItem(), stage, manager);
            });
            list.setOnKeyPressed(event -> {
                if (event.getCode() == KeyCode.ENTER) login.fire();
            });
            list.setCellFactory(param -> new ListCell<String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty) {
                        setText(item);
                        setOnMouseClicked(mouseEvent -> {
                            if (mouseEvent.getButton() == MouseButton.PRIMARY && mouseEvent.getClickCount() == 2) login.fire();
                        });
                    } else {
                        setText(null);
                        setOnMouseClicked(null);
                    }
                }
            });
            ObservableList<String> items = list.getItems();
            for (Manager.User user : manager.users) items.add(user.name);
            if (!items.isEmpty()) list.getSelectionModel().select(0);
        }
        ((Button) scene.lookup("#buttonSignup")).setOnAction(event -> signup(stage, manager));
        stage.show();
    }

    private static void login(String username, Stage owner, Manager manager) {
        Stage stage = stage("Enter password:", owner);
        Scene scene = scene("dialog/login.fxml", stage);
        ((Label) scene.lookup("#username")).setText(username);
        TextField passwordField = (TextField) scene.lookup("#password");
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        ((Button) scene.lookup("#login")).setOnAction(event -> {
            if (manager.login(username, passwordField.getText())) {
                stage.close();
                owner.close();
            } else message(owner, "Incorrect password", "The password you entered is incorrect. Please try again.");
        });
        stage.show();
    }

    private static void signup(Stage owner, Manager manager) {
        Stage stage = stage("Create new folder", owner);
        Scene scene = scene("dialog/signup.fxml", stage);
        TextField fieldUser = (TextField) scene.lookup("#fieldUsername"),
                fieldPassword = (TextField) scene.lookup("#fieldPassword"),
                fieldConfirm = (TextField) scene.lookup("#fieldConfirm");
        Label status = (Label) scene.lookup("#status");
        Button button = ((Button) scene.lookup("#buttonSignup"));
        button.setDisable(true);
        ChangeListener<String> listener = (observable, oldValue, newValue) -> {
            String username = fieldUser.getText().trim(), password = fieldPassword.getText(), confirm = fieldConfirm.getText();
            if (Utils.checkUsername(username)) status.setText("Username contains illegal characters");
            else if (username.length() < 4) status.setText("Username too short");
            else if (username.length() > 16) status.setText("Username too long");
            else if (password.length() < 8) status.setText("Password too short");
            else if (password.length() > 64) status.setText("Password too long");
            else if (!password.equals(confirm)) status.setText("Passwords do not match");
            else {
                status.setText(null);
                button.setDisable(false);
                return;
            }
            button.setDisable(true);
        };
        fieldUser.textProperty().addListener(listener);
        fieldPassword.textProperty().addListener(listener);
        fieldConfirm.textProperty().addListener(listener);
        button.setOnAction(event -> {
            String username = fieldUser.getText().trim(), password = fieldPassword.getText();
            if (!manager.signup(username, password)) {
                message(owner, "Username already exists", "Username already in use. Please enter a new name.");
                return;
            }
            stage.close();
            stage.getOwner().hide();
            manager.login(username, password);
        });
        ((Button) scene.lookup("#buttonCancel")).setOnAction(event -> stage.close());
        stage.show();
    }

    // Account edit

    static void changeUsername(Stage owner, Manager manager) {
        Stage stage = stage("Change folder name", owner);
        Scene scene = scene("dialog/changeUsername.fxml", stage);
        TextField newName = (TextField) scene.lookup("#username"), password = (TextField) scene.lookup("#password");
        ((Label) scene.lookup("#oldName")).setText(manager.currentUser.name);
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        Label status = (Label) scene.lookup("#status");
        Button button = ((Button) scene.lookup("#change"));
        button.setDisable(true);
        newName.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.trim();
            if (Utils.checkUsername(newValue)) status.setText("Username contains illegal characters");
            else if (newValue.length() < 4) status.setText("Username is too short");
            else if (newValue.length() > 16) status.setText("Username is too long");
            else {
                button.setDisable(false);
                status.setText(null);
                return;
            }
            button.setDisable(true);
        });
        button.setOnAction(event -> {
            switch (manager.changeUsername(newName.getText().trim(), password.getText())) {
                case 1:
                    message(owner, "Incorrect password", "The password you entered is incorrect. Please try again.");
                    break;
                case 2:
                    message(owner, "Username already exists", "Username already in use. Please enter a new name.");
                    break;
                default:
                    message(owner, "Success", "Username changed successfully.");
                    stage.close();
                    break;
            }
        });
        stage.show();
    }

    static void changePassword(Stage owner, Manager manager) {
        Stage stage = stage("Change password", owner);
        Scene scene = scene("dialog/changePassword.fxml", stage);
        TextField fieldPassword = (TextField) scene.lookup("#password"), fieldConfirm = (TextField) scene.lookup("#confirm"), fieldOld = (TextField) scene.lookup("#old");
        ((Label) scene.lookup("#username")).setText(manager.currentUser.name);
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        Label status = (Label) scene.lookup("#status");
        Button button = ((Button) scene.lookup("#change"));
        button.setDisable(true);
        ChangeListener<String> listener = (observable, oldValue, newValue) -> {
            if (newValue.length() < 8) status.setText("Password is too short");
            else if (newValue.length() > 64) status.setText("Password is too long");
            else if (!newValue.equals(fieldConfirm.getText())) status.setText("Passwords do not match");
            else {
                button.setDisable(false);
                status.setText(null);
                return;
            }
            button.setDisable(true);
        };
        fieldPassword.textProperty().addListener(listener);
        fieldConfirm.textProperty().addListener(listener);
        button.setOnAction(event -> {
            if (manager.changePassword(fieldPassword.getText(), fieldOld.getText())) {
                message(owner, "Success", "Password changed successfully.");
                stage.close();
            } else message(owner, "Incorrect password", "The password you entered is incorrect. Please try again.");
        });
        stage.show();
    }

    static void deleteAccount(Stage owner, Manager manager) {
        Stage stage = stage("Delete private folder", owner);
        Scene scene = scene("dialog/deleteAccount.fxml", stage);
        TextField fieldPassword = (TextField) scene.lookup("#password");
        ((Label) scene.lookup("#username")).setText(manager.currentUser.name);
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        ((Button) scene.lookup("#delete")).setOnAction(event -> {
            if (manager.deleteAccount(fieldPassword.getText())) {
                message(owner, "Success", "User account deleted successfully");
                stage.close();
            } else message(owner, "Incorrect password", "The password you entered is incorrect. Please try again.");
        });
        stage.show();
    }

    // Toolbar

    static void about(Stage owner, Main main) {
        Stage stage = stage("About", owner);
        Scene scene = scene("dialog/about.fxml", stage);
        ((Button) scene.lookup("#close")).setOnAction(event -> stage.close());
        ((Hyperlink) scene.lookup("#link")).setOnAction(event -> main.getHostServices().showDocument("http://icons8.com"));
        ((Hyperlink) scene.lookup("#license")).setOnAction(event -> main.getHostServices().showDocument("http://creativecommons.org/licenses/by-nd/3.0/legalcode"));
        stage.show();
    }

    static void status(Stage owner, Manager manager) {
        Stage stage = stage("Current status", owner);
        Scene scene = scene("dialog/status.fxml", stage);
        String[] openFiles = manager.getOpenFiles();
        ((Button) scene.lookup("#button")).setOnAction(event -> stage.close());
        VBox vbox = (VBox) ((ScrollPane) scene.lookup("#scroll")).getContent();
        ((Label) vbox.lookup("#viewingCount")).setText(openFiles[0].isEmpty() ? "0" : String.valueOf(Utils.countLines(openFiles[0])));
        ((Label) vbox.lookup("#editingCount")).setText(openFiles[1].isEmpty() ? "0" : String.valueOf(Utils.countLines(openFiles[1])));
        ((Label) vbox.lookup("#viewing")).setText(openFiles[0]);
        ((Label) vbox.lookup("#editing")).setText(openFiles[1]);
        stage.show();
    }

    static void fullCleanup(Stage owner, Manager manager) {
        Stage stage = stage("Full cleanup", owner);
        Scene scene = scene("dialog/fullCleanup.fxml", stage);
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        ((Button) scene.lookup("#cleanup")).setOnAction(event -> {
            stage.close();
            manager.fullCleanup(owner);
        });
        stage.show();
    }

    // File manager

    static void create(Stage owner, Manager manager) {
        Stage stage = stage("Create new...", owner);
        Scene scene = scene("dialog/create.fxml", stage);
        //noinspection unchecked
        ChoiceBox<String> choiceBox = (ChoiceBox<String>) scene.lookup("#choices");
        choiceBox.getItems().setAll("New folder", "New file");
        choiceBox.getSelectionModel().select(0);
        ((Button) scene.lookup("#create")).setOnAction(event -> {
            stage.close();
            newFileOrFolder(owner, manager, choiceBox.getSelectionModel().getSelectedIndex() == 1);
        });
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        stage.show();
    }

    static void rename(Stage owner, Manager manager, Manager.File file) {
        Stage stage = stage(file instanceof Manager.Folder ? "Rename folder" : "Rename file", owner);
        Scene scene = scene("dialog/rename.fxml", stage);
        ((Label) scene.lookup("#filename")).setText(file.name);
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        TextField field = (TextField) scene.lookup("#field");
        field.setText(file.name);
        String[] oldName = Utils.splitFilename(file.name);
        Button button = ((Button) scene.lookup("#rename"));
        button.setDisable(true);
        field.textProperty().addListener((observable, oldValue, newValue) -> button.setDisable(Utils.checkFilename(newValue.trim())));
        button.setOnAction(event -> {
            String name = field.getText().trim();
            if (!oldName[1].equals(Utils.splitFilename(name)[1]) && Dialogs.areYouSure(owner, "Are you sure?", "Are you sure you want to change the file extension?", 0, 1) != 1)
                return;
            if (Utils.checkFilename(name)) {
                button.setDisable(true);
            } else {
                if (manager.rename(file, name)) stage.close();
                else
                    Dialogs.message(owner, "Already exists", "A file or folder with that name already exists. Please specify a new name.");
            }
        });
        stage.show();
        field.selectRange(0, oldName[0].length());
    }

    static void deleteFiles(Stage owner, Manager manager, List<Manager.File> files) {
        Stage stage = stage("Are you sure?", owner);
        Scene scene = scene("dialog/deleteFiles.fxml", stage);
        ((Label) scene.lookup("#count")).setText(String.valueOf(files.size()));
        ((Button) scene.lookup("#delete")).setOnAction(event -> {
            Manager.File f = manager.deleteFiles(files);
            if (f != null)
                Dialogs.message(owner, "Error while deleting file", "Unable to delete the following file: " + f.name + "\nDisk location:\n" + f.getFile());
            stage.close();
        });
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        stage.showAndWait();
    }

    static void newFileOrFolder(Stage owner, Manager manager, boolean file) {
        Stage stage = stage(file ? "New File" : "New Folder", owner);
        Scene scene = scene("dialog/newFolderFile.fxml", stage);
        ((Label) scene.lookup("#type")).setText(file ? "file" : "folder");
        TextField name = (TextField) scene.lookup("#name");
        Button create = ((Button) scene.lookup("#create"));
        create.setDisable(true);
        name.textProperty().addListener((observable, oldValue, newValue) -> create.setDisable(Utils.checkFilename(newValue.trim())));
        ((Button) scene.lookup("#cancel")).setOnAction(event1 -> stage.close());
        create.setOnAction(event1 -> {
            String text = name.getText().trim();
            if (Utils.checkFilename(text)) {
                create.setDisable(true);
            } else {
                if (file ? manager.newFile(text) : manager.newFolder(text)) stage.close();
                else
                    Dialogs.message(owner, "Already exists", "A file or folder with that name already exists. Please specify a new name.");
            }
        });
        stage.show();
    }

    /**
     * @return -1 for move, 1 for copy, 0 for cancelled
     */
    static int copyOrMove(Stage owner) {
        Stage stage = stage("Copy or move", owner);
        Scene scene = scene("dialog/copyOrMove.fxml", stage);
        AtomicInteger result = new AtomicInteger();
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        ((Button) scene.lookup("#copy")).setOnAction(event -> {
            result.set(1);
            stage.close();
        });
        ((Button) scene.lookup("#move")).setOnAction(event -> {
            result.set(-1);
            stage.close();
        });
        stage.showAndWait();
        return result.get();
    }

    // Password manager

    static PasswordManager.PasswordEntry editEntry(Stage owner, PasswordManager.PasswordEntry existing, Set<String> websites, BiPredicate<String, String> exists) {
        // Setup stage
        PasswordManager.PasswordEntry entry = new PasswordManager.PasswordEntry();
        Stage stage = stage(existing == null ? "Create entry" : "Edit entry", owner);
        Scene scene = scene("dialog/createPasswordEntry.fxml", stage);
        // Setup fields
        //noinspection unchecked
        ComboBox<String> website = (ComboBox<String>) scene.lookup("#website");
        website.getItems().setAll(websites);
        TextField username = (TextField) scene.lookup("#username");
        PasswordField password = (PasswordField) scene.lookup("#password");
        TextField passwordVisible = (TextField) scene.lookup("#passwordVisible");
        passwordVisible.setManaged(false);
        passwordVisible.setVisible(false);
        password.textProperty().bindBidirectional(passwordVisible.textProperty());
        // Set values if needed
        if (existing != null) {
            website.setValue(existing.getWebsite());
            username.setText(existing.getUsername());
            password.setText(existing.getPassword());
        }
        // Show password checkbox
        ((CheckBox) scene.lookup("#show")).selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                passwordVisible.setManaged(true);
                passwordVisible.setVisible(true);
                password.setManaged(false);
                password.setVisible(false);
            } else {
                passwordVisible.setManaged(false);
                passwordVisible.setVisible(false);
                password.setManaged(true);
                password.setVisible(true);
            }
        });
        // Other UI elements
        Label status = (Label) scene.lookup("#status");
        Button create = (Button) scene.lookup("#create");
        if (existing != null) create.setText("Save");
        create.setDisable(true);
        // Check if data is valid whenever it changes
        ChangeListener<String> listener = (observable, oldValue, newValue) -> {
            if (website.getEditor().getText().isEmpty()) status.setText("Website can't be empty");
            else if (username.getText().isEmpty()) status.setText("Username can't be empty");
            else if (existing != null && !existing.getWebsite().equals(website.getEditor().getText()) && !existing.getUsername().equals(username.getText()) &&
                    exists.test(website.getEditor().getText(), username.getText())) status.setText("Website-username combination already exists");
            else {
                status.setText(null);
                create.setDisable(false);
                return;
            }
            create.setDisable(true);
        };
        website.getEditor().textProperty().addListener(listener);
        username.textProperty().addListener(listener);
        password.textProperty().addListener(listener);
        // When user clicks the create / edit button
        create.setOnAction(event -> {
            // If nothing changed, don't update
            if (existing == null || !existing.getWebsite().equals(website.getValue()) || !existing.getUsername().equals(username.getText()) ||
                    !existing.getPassword().equals(password.getText())) {
                // Otherwise set the changes
                entry.setWebsite(website.getValue());
                entry.setUsername(username.getText());
                entry.setPassword(password.getText());
            }
            stage.close();
        });
        // Show scene
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        stage.setScene(scene);
        stage.showAndWait();
        // Return the password entry if needed
        if (entry.getPassword() == null) return null;
        else return entry;
    }

    // Alerts

    static void message(Stage owner, String title, String message) {
        Stage stage = stage(title, owner);
        stage.initModality(Modality.APPLICATION_MODAL);
        Scene scene = scene("dialog/message.fxml", stage);
        ((Label) scene.lookup("#message")).setText(message);
        ((Button) scene.lookup("#button")).setOnAction(event -> stage.close());
        stage.showAndWait();
    }

    /**
     * Returns 1 for yes, -1 for no, 0 for cancel<br/>
     * 1  = YES<br/>
     * -1 = NO<br/>
     * Anything else = neither
     */
    static int areYouSure(Stage owner, String title, String message, int red, int blue) {
        Stage stage = stage(title, owner);
        stage.initModality(Modality.APPLICATION_MODAL);
        Scene scene = scene("dialog/areYouSure.fxml", stage);
        ((Label) scene.lookup("#message")).setText(message);
        AtomicInteger result = new AtomicInteger();
        Button b;
        b = ((Button) scene.lookup("#yes"));
        if (red == 1) b.setStyle("-fx-base: ff8888;");
        if (blue == 1) b.setDefaultButton(true);
        b.setOnAction(event -> {
            result.set(1);
            stage.close();
        });
        b = ((Button) scene.lookup("#no"));
        if (red == -1) b.setStyle("-fx-base: ff8888;");
        if (blue == -1) b.setDefaultButton(true);
        b.setOnAction(event -> {
            result.set(-1);
            stage.close();
        });
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        stage.showAndWait();
        return result.get();
    }

    // Utils

    static Stage stage(String title, Stage owner) {
        Stage stage = new Stage();
        stage.getIcons().setAll(Main.ICONS);
        stage.setResizable(false);
        stage.initOwner(owner);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setTitle(title);
        if (owner != null) stage.setOnShown(event -> {
            stage.setX(owner.getX() + owner.getWidth() / 2 - stage.getWidth() / 2);
            stage.setY(owner.getY() + owner.getHeight() / 2 - stage.getHeight() / 2);
        });
        return stage;
    }

    static Scene scene(String filename, Stage stage) {
        try {
            Scene scene = new Scene(FXMLLoader.load(Dialogs.class.getResource(filename)));
            stage.setScene(scene);
            return scene;
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    static void securityException(Stage owner, boolean file, String path, SecurityException e) {
        message(owner, "No permission", "Security error: no permission to access " + (file ? "file: " : "folder: ") + path + "\n" + e);
    }

    // Async task dialog

    static abstract class AsyncTask<R> {

        final Stage dialog;
        private boolean canceled;

        AsyncTask(Stage owner, String title, String message, boolean closeable) {
            dialog = stage(title, owner);
            // Set closeable
            if (!closeable) dialog.setOnCloseRequest(Event::consume);
            else dialog.setOnCloseRequest(event -> cancel());
            // Load & initialize scene
            Scene scene = scene("AsyncTask.fxml", dialog);
            ((Label) scene.lookup("#info")).setText(message);
            // Enable / disable cancel button based on closeable
            if (closeable) ((Button) scene.lookup("#cancel")).setOnAction(event -> cancel());
            else scene.lookup("#cancel").setDisable(true);
        }

        // Will run on FX thread and waits for result
        <T> T runOnFxThreadAndWait(Supplier<T> r) {
            AtomicBoolean lock = new AtomicBoolean(true);
            AtomicReference<T> result = new AtomicReference<>();
            Platform.runLater(() -> {
                try {
                    result.set(r.get());
                } finally {
                    lock.set(false);
                    synchronized (lock) {
                        lock.notify();
                    }
                }
            });
            //noinspection Duplicates
            while (lock.get()) {
                synchronized (lock) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            return result.get();
        }

        void runOnFxThreadAndWait(Runnable r) {
            AtomicBoolean lock = new AtomicBoolean(true);
            Platform.runLater(() -> {
                try {
                    r.run();
                } finally {
                    lock.set(false);
                    synchronized (lock) {
                        lock.notify();
                    }
                }
            });
            //noinspection Duplicates
            while (lock.get()) {
                synchronized (lock) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        R start(boolean wait) {
            beforeRun();
            if (isCancelled()) return null;
            AtomicReference<R> reference = new AtomicReference<>();
            new Thread(() -> {
                R result = null;
                try {
                    result = run();
                } finally {
                    reference.set(result);
                    Platform.runLater(dialog::close);
                    R result1 = result;
                    Platform.runLater(() -> afterRun(result1));
                }
            }).start();
            if (wait) dialog.showAndWait();
            else dialog.show();
            return reference.get();
        }

        boolean isCancelled() {
            return canceled;
        }

        void checkCancelled() throws OperationCancelled {
            if (isCancelled()) throw new OperationCancelled();
        }

        void cancel() {
            canceled = true;
        }

        abstract R run();

        void afterRun(R result) {
        }

        void beforeRun() {
        }

    }

    // Used to cancel recursive methods completely
    static final class OperationCancelled extends Exception {
    }
}
