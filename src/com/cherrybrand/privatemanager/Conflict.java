package com.cherrybrand.privatemanager;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class Conflict {

    private static final Image iconFile, iconFolder;

    static {
        iconFile = new Image(Conflict.class.getResourceAsStream("img/file.png"));
        iconFolder = new Image(Conflict.class.getResourceAsStream("img/folder.png"));
    }

    /**
     * First = existing
     * Second = new
     */
    enum ConflictType {
        FOLDER_FOLDER,
        FILE_FILE,
        FILE_FOLDER,
        FOLDER_FILE
    }

    static class OccurredConflicts {

        private ConflictResult folderFolder, fileFile, fileFolder, folderFile;

        ConflictResult getResult(ConflictType type) {
            switch (type) {
                case FILE_FILE: return fileFile;
                case FOLDER_FOLDER: return folderFolder;
                case FILE_FOLDER: return fileFolder;
                case FOLDER_FILE: return folderFile;
                default: throw new AssertionError(type);
            }
        }

        void onConflict(ConflictType type, ConflictResult result) {
            if (result.shouldApplyToAll()) {
                switch (type) {
                    case FILE_FILE:
                        fileFile = result;
                        break;
                    case FOLDER_FOLDER:
                        folderFolder = result;
                        break;
                    case FILE_FOLDER:
                        fileFolder = result;
                        break;
                    case FOLDER_FILE:
                        folderFile = result;
                        break;
                }
            }
        }

    }

    static class ConflictResult {

        static final int RENAME_EXISTING = 1, RENAME_NEW = 2, SKIP = 3, MERGE_OR_REPLACE = 4, CANCEL = 5;
        private int result;
        private boolean applyToAll;

        private ConflictResult() {
        }

        int getResult() {
            return result;
        }

        boolean shouldApplyToAll() {
            return applyToAll;
        }

    }

    static ConflictResult show(Stage owner, ConflictType type, String existingName, String newName) {
        // Create stage
        Stage stage = Dialogs.stage("File conflict", owner);
        Conflict controller;
        // Load fxml
        try {
            FXMLLoader loader = new FXMLLoader(Conflict.class.getResource("Conflict.fxml"));
            stage.setScene(new Scene(loader.load()));
            controller = loader.getController();
        } catch (Exception e) {
            throw new Error(e);
        }
        // Initialize stuff in the controller
        ConflictResult result = new ConflictResult();
        controller.init(stage, type, result, existingName, newName);
        // Wait for result and then return result
        stage.showAndWait();
        return result;
    }

    @FXML
    private Label info, existingType, newType, existingFile, newFile;
    @FXML
    private ImageView existingImage, newImage;
    @FXML
    private Button mergeOrReplace, renameExisting, renameNew;
    @FXML
    private CheckBox applyToAll;

    private Stage stage;
    private ConflictResult result;

    private void init(Stage stage, ConflictType type, ConflictResult result, String existingName, String newName) {
        // Initialize stuff that doesn't depend on ConflictType
        this.stage = stage;
        this.result = result;
        existingFile.setText(existingName);
        newFile.setText(existingName);
        applyToAll.selectedProperty().addListener((observable, oldValue, newValue) -> this.result.applyToAll = newValue);
        // Initialize based on conflict type
        switch (type) {
            case FILE_FILE: // Existing file, new file
                info.setText("A file with the same name already exists. How would you like to proceed?");
                mergeOrReplace.setText("Replace existing file");
                existingType.setText("Existing file");
                newType.setText("New file");
                existingImage.setImage(iconFile);
                newImage.setImage(iconFile);
                renameExisting.setText("Rename existing file to:\n" + newName);
                renameNew.setText("Rename new file to:\n" + newName);
                break;
            case FOLDER_FOLDER: // Existing folder, new folder
                info.setText("A folder with the same name already exists. How would you like to proceed?");
                mergeOrReplace.setText("Merge folders");
                existingType.setText("Existing folder");
                newType.setText("New folder");
                existingImage.setImage(iconFolder);
                newImage.setImage(iconFolder);
                renameExisting.setText("Rename existing folder to:\n" + newName);
                renameNew.setText("Rename new folder to:\n" + newName);
                break;
            case FILE_FOLDER: // Existing file, new folder
                info.setText("You are trying to add a file but a folder with the same name already exists. How would you like to proceed?");
                mergeOrReplace.setText("Delete existing folder");
                existingType.setText("Existing folder");
                newType.setText("New file");
                existingImage.setImage(iconFolder);
                newImage.setImage(iconFile);
                renameExisting.setText("Rename existing file to:\n" + newName);
                renameNew.setText("Rename new folder to:\n" + newName);
                break;
            case FOLDER_FILE: // Existing folder, new file
                info.setText("You are trying to add a folder but a folder with the same name already exists. How would you like to proceed?");
                mergeOrReplace.setText("Delete existing file");
                existingType.setText("Existing file");
                newType.setText("New folder");
                existingImage.setImage(iconFile);
                newImage.setImage(iconFolder);
                renameExisting.setText("Rename existing folder to:\n" + newName);
                renameNew.setText("Rename new file to:\n" + newName);
                break;
        }
    }

    @FXML
    public void renameExisting() {
        result.result = ConflictResult.RENAME_EXISTING;
        stage.close();
    }

    @FXML
    public void renameNew() {
        result.result = ConflictResult.RENAME_NEW;
        stage.close();
    }

    @FXML
    public void skip() {
        result.result = ConflictResult.SKIP;
        stage.close();
    }

    @FXML
    public void mergeOrReplace() {
        result.result = ConflictResult.MERGE_OR_REPLACE;
        stage.close();
    }

    public void cancel() {
        result.result = ConflictResult.CANCEL;
        stage.close();
    }
}
