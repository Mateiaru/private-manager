package com.cherrybrand.privatemanager;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Advanced file selector
class FileSelector {

    // Used for decrypting (removing)
    static void show(Stage owner, Main main, List<Manager.File> selection) {
        // Create and initialize the stage
        Stage stage = Dialogs.stage("Advanced file selector", owner);
        stage.setMinWidth(400);
        stage.setMinHeight(300);
        stage.setResizable(true);
        // Load the scene and set controller
        FXMLLoader loader = new FXMLLoader(FileSelector.class.getResource("FileSelector.fxml"));
        loader.setController(new FileSelector(stage, main, new ArrayList<>(selection)));
        Scene scene;
        try {
            scene = new Scene(loader.load());
        } catch (Exception e) {
            throw new Error(e);
        }
        // Show the stage
        stage.setScene(scene);
        stage.show();
    }

    // Used for encrypting (adding)
    static void show(Stage owner, Main main, Path folder) {
        // Create and initialize the stage
        Stage stage = Dialogs.stage("Advanced file selector", owner);
        stage.setMinWidth(400);
        stage.setMinHeight(300);
        stage.setResizable(true);
        // Load the scene and set controller
        FXMLLoader loader = new FXMLLoader(FileSelector.class.getResource("FileSelector.fxml"));
        try {
            loader.setController(new FileSelector(stage, main, folder));
        } catch (Exception e) {
            if (e instanceof SecurityException) Dialogs.securityException(owner, false, folder.toString(), (SecurityException) e);
            else main.error("Unable to access folder: " + folder, e);
            return;
        }
        Scene scene;
        try {
            scene = new Scene(loader.load());
        } catch (Exception e) {
            throw new Error(e);
        }
        // Show the stage
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private Label info;
    @FXML
    private Label selectedFiles;
    @FXML
    private TextField maximumDepth;
    @FXML
    private ChoiceBox<String> fileExtensions;
    @FXML
    private Button fileExtensionsButton;
    @FXML
    private TextField regex;
    private String extensionsList = "";
    @FXML
    private CheckBox excludeExtension, emptyExtension;
    @FXML
    private Spinner<Integer> minimumSize, maximumSize;

    private final Stage stage;
    private final Main main;
    private final List<Manager.File> selectionRemove;
    private final List<Path> selectionAdd;

    // Used for removing
    private FileSelector(Stage stage, Main main, List<Manager.File> selection) {
        this.stage = stage;
        this.main = main;
        this.selectionRemove = selection;
        this.selectionAdd = null;
    }

    // Used for adding
    private FileSelector(Stage stage, Main main, Path folder) throws Exception {
        this.stage = stage;
        this.main = main;
        this.selectionRemove = null;
        try (Stream<Path> stream = Files.list(folder)) {
            selectionAdd = stream.collect(Collectors.toList());
        }
    }

    @FXML
    public void initialize() {
        // Stuff that is independent on whether you are adding / removing
        // Make sure maximum depth is a number
        maximumDepth.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty() || newValue.equals("-")) return;
            if (newValue.startsWith("0") && newValue.length() > 1 || newValue.startsWith("-0") && newValue.length() > 2) maximumDepth.setText(oldValue);
            try {
                if (Math.abs(Integer.parseInt(newValue)) > 999) maximumDepth.setText(oldValue);
            } catch (NumberFormatException e) {
                maximumDepth.setText(oldValue);
            }
        });
        // File extensions
        fileExtensions.getItems().setAll("All", "Whitelist", "Blacklist");
        fileExtensions.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> fileExtensionsButton.setDisable(newValue.intValue() == 0));
        fileExtensions.getSelectionModel().select(0);
        Stage stage = Dialogs.stage("Edit list", this.stage);
        Scene scene = Dialogs.scene("FileSelectorExtensions.fxml", stage);
        TextField field = (TextField) scene.lookup("#extensions");
        ((Button) scene.lookup("#cancel")).setOnAction(event -> stage.close());
        ((Button) scene.lookup("#ok")).setOnAction(event -> {
            extensionsList = field.getText();
            stage.close();
        });
        fileExtensionsButton.setOnAction(event -> stage.show());
        // Minimum / maximum size settings
        minimumSize.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1048576, 0));
        minimumSize.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.startsWith("0") && newValue.length() > 1) minimumSize.getEditor().setText(oldValue);
            try {
                if (Integer.parseInt(newValue) > 1048576) minimumSize.getEditor().setText("1048576");
            } catch (NumberFormatException e) {
                minimumSize.getEditor().setText(oldValue);
            }
        });
        maximumSize.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1048576, 1048576));
        maximumSize.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.startsWith("00")) maximumSize.getEditor().setText(oldValue);
            try {
                if (Integer.parseInt(newValue) > 1048576) maximumSize.getEditor().setText("1048576");
            } catch (NumberFormatException e) {
                maximumSize.getEditor().setText(oldValue);
            }
        });

        // Decrypting
        if (selectionRemove != null) {
            // Set info text
            if (selectionRemove.get(0).parent.parent == null) info.setText("Decrypting files from private folder:");
            else info.setText("Decrypting files from \'" + selectionRemove.get(0).parent.name + "\':");
            // Set selection text
            StringBuilder builder = new StringBuilder();
            selectionRemove.sort(null);
            for (Manager.File file : selectionRemove) {
                builder.append(file.name);
                if (file instanceof Manager.Folder) builder.append('/');
                builder.append('\n');
            }
            builder.setLength(builder.length() - 1);
            selectedFiles.setText(builder.toString());

        } else {
            // Encrypting
            // Set info text
            info.setText("Encrypting files:");
            // Set selection text
            StringBuilder builder = new StringBuilder();
            selectionAdd.sort(Comparator.comparing(o -> o.getFileName().toString()));
            for (Path path : selectionAdd) {
                builder.append(path.getFileName().toString());
                try {
                    if (Files.isDirectory(path)) builder.append('/');
                } catch (SecurityException e) {
                    stage.close();
                    Dialogs.securityException((Stage) stage.getOwner(), true, path.toString(), e);
                    return;
                }
                builder.append('\n');
            }
            builder.setLength(builder.length() - 1);
            selectedFiles.setText(builder.toString());
        }
    }

    @FXML
    public void apply() {
        new AsyncPreview().start(false);
    }

    private class AsyncPreview extends Dialogs.AsyncTask<TreeDialog.TreeNode> {

        private AsyncPreview() {
            super(stage, "Calculating preview...", "Please wait, processing file tree...", true);
        }

        private int maxDepth, minSize, maxSize;
        private boolean useExtensions, allowEmptyExtension, usePattern, patternExcludeExtensions, whitelist;
        private List<String> extensions;
        private Pattern pattern;
        private String error;

        @Override
        void beforeRun() {
            // Obtain all filters
            String s;
            // Filename regex
            s = regex.getText();
            if (s.isEmpty()) usePattern = false;
            else {
                usePattern = true;
                try {
                    pattern = Pattern.compile(s);
                } catch (PatternSyntaxException e) {
                    Dialogs.message(stage, "Invalid regex", "Invalid filename regex, please check the regular expression and try again.");
                    cancel();
                    return;
                }
            }
            patternExcludeExtensions = excludeExtension.isSelected();
            // Min / max sizes, max depth
            minSize = minimumSize.getValue();
            maxSize = maximumSize.getValue();
            maxDepth = Integer.parseInt(maximumDepth.getText());
            if (maxDepth < 0) maxDepth = Integer.MAX_VALUE;
            // File extensions
            allowEmptyExtension = emptyExtension.isSelected();
            switch (fileExtensions.getSelectionModel().getSelectedIndex()) {
                // Case 0 is not needed as useExtensions is false by default
                case 1: // Whitelist
                    whitelist = true;
                case 2: // Blacklist
                    useExtensions = true;
                    if (extensionsList.isEmpty()) extensions = Collections.emptyList();
                    else extensions = Stream.of(extensionsList.split(",")).map("."::concat).collect(Collectors.toList());
                    break;
            }
        }

        @Override
        TreeDialog.TreeNode run() {
            TreeDialog.TreeNode root;
            if (selectionRemove != null) { // Removing
                root = new TreeDialog.TreeNodeFile(null);
                // Process all selected files and folders
                try {
                    for (Manager.File file : selectionRemove) processFile(file, 0, (TreeDialog.TreeNodeFile) root);
                } catch (Dialogs.OperationCancelled e) {
                    return null;
                }
            } else { // Adding
                root = new TreeDialog.TreeNodePath();
                // Create a File Tree for all selected file
                try {
                    for (Path path : selectionAdd) processPath(path, 0, (TreeDialog.TreeNodePath) root);
                } catch (Dialogs.OperationCancelled e) {
                    return null;
                }
            }
            return root;
        }

        // Recursive method
        private void processFile(Manager.File file, int depth, TreeDialog.TreeNodeFile parent) throws Dialogs.OperationCancelled {
            checkCancelled();
            if (file instanceof Manager.Folder) {
                // If file is a folder, process all sub-folders / files
                if (depth < maxDepth) {
                    depth++;
                    // Create a new folder in the Tree
                    TreeDialog.TreeNodeFile item = new TreeDialog.TreeNodeFile(file);
                    // Process files
                    ((Manager.Folder) file).sort(); // Make sure files are sorted
                    for (Manager.File f : ((Manager.Folder) file)) processFile(f, depth, item);
                    // Only add the folder if it has children
                    if (!item.children.isEmpty()) parent.add(item);
                }
                // If maximum depth was reached, don't do anything
                return;
            }
            try {
                // If all filters are passed, add the file to the tree
                if (checkFilters(file.name, file.size())) parent.add(new TreeDialog.TreeNodeFile(file));
            } catch (Exception e) {
                error = "Unable to access path: " + file.getFile() + "\n" + e.getMessage();
                throw new Dialogs.OperationCancelled();
            }
        }

        // Recursive method
        private void processPath(Path path, int depth, TreeDialog.TreeNodePath parent) throws Dialogs.OperationCancelled {
            checkCancelled();
            try {
                if (Files.isDirectory(path)) {
                    // If file is a folder, process all sub-folders / files
                    if (depth < maxDepth) {
                        depth++;
                        // Create a new folder in the Tree
                        TreeDialog.TreeNodePath item = new TreeDialog.TreeNodePath(path);
                        // Process files
                        List<Path> files;
                        try (Stream<Path> stream = Files.list(path)) {
                            files = stream.collect(Collectors.toCollection(ArrayList::new));
                        }
                        for (Path p : files) processPath(p, depth, item);
                        // Only add the folder if it has children
                        if (!item.children.isEmpty()) parent.add(item);
                    }
                    // If maximum depth was reached, don't do anything
                    return;
                }
                // If all filters are passed, add the file to the tree
                if (checkFilters(path.getFileName().toString(), Files.size(path))) parent.add(new TreeDialog.TreeNodePath(path));
            } catch (IOException | SecurityException e) { // Don't catch OperationCancelled
                error = "Error, cannot access path: " + path + "\n" + e.getMessage();
                throw new Dialogs.OperationCancelled();
            }
        }

        private boolean checkFilters(String filename, long fileSize) {
            // Obtain name & extension
            String[] name = Utils.splitFilename(filename);
            if (name[1].isEmpty()) {
                if (!allowEmptyExtension) return false;
            } else if (useExtensions) {
                // Check if extension is in user's white/blacklist
                if (whitelist) {
                    if (!extensions.contains(name[1])) return false;
                } else {
                    if (extensions.contains(name[1])) return false;
                }
            }
            // Check pattern if using one
            if (usePattern) {
                if (!pattern.matcher(patternExcludeExtensions ? name[0] : filename).matches()) return false;
            }
            // Test file sizes
            int size = (int) (fileSize / 1048576); // 1024 * 1024
            return size >= minSize && size <= maxSize;
        }

        @Override
        void afterRun(TreeDialog.TreeNode root) {
            // This runs on FX thread
            if (error != null) {
                Dialogs.message(stage, "Error processing files", error);
                return;
            }
            if (isCancelled()) return; // If user canceled, don't show anything
            if (root.children.isEmpty()) Dialogs.message(stage, "No files matched", "No files matched all the specified filters.");
            else {
                TreeDialog.TreeNode treeNodeResult = TreeDialog.show(stage, root, false);
                if (treeNodeResult.children.isEmpty()) return;
                if (treeNodeResult instanceof TreeDialog.TreeNodeFile) {
                    File folder = main.chooseRemoveDirectory(stage);
                    if (folder == null) return;
                    // Ask whether to copy or move the files
                    int copyOrMove = Dialogs.copyOrMove(stage);
                    if (copyOrMove == 0) return;
                    Path dest = folder.toPath();
                    stage.close();
                    List<TreeDialog.TreeNodeFile> files = treeNodeResult.getChildren();
                    main.new AsyncRemove<TreeDialog.TreeNodeFile>(copyOrMove, files) {
                        @Override
                        void removeFile(TreeDialog.TreeNodeFile file) throws Dialogs.OperationCancelled {
                            removeFile(dest, file);
                        }

                        // Move a file outside from the private folder
                        @SuppressWarnings("Duplicates")
                        private void removeFile(Path folder, TreeDialog.TreeNodeFile file) throws Dialogs.OperationCancelled {
                            checkCancelled();
                            try {
                                Files.createDirectories(folder);
                            } catch (Exception e) {
                                exception = e;
                                this.file = false;
                                this.exceptionFile = folder.toString();
                                this.exceptionText = "Unable to create folder: " + exceptionFile;
                                throw new Dialogs.OperationCancelled();
                            }
                            Path dest = folder.resolve(file.getFile().name);
                            try {
                                if (Files.exists(dest)) {
                                    // Check what kind of conflict we have
                                    String oldName = file.getFile().name, newName = Utils.findFilename(oldName, s -> !Files.exists(folder.resolve(s)));
                                    boolean oldDir = file.getFile() instanceof Manager.Folder, newDir = Files.isDirectory(dest);
                                    // Perform action based on result
                                    switch (onConflict(oldName, newName, oldDir, newDir).getResult()) {
                                        case Conflict.ConflictResult.CANCEL:
                                            throw new Dialogs.OperationCancelled();
                                        case Conflict.ConflictResult.SKIP:
                                            return;
                                        case Conflict.ConflictResult.MERGE_OR_REPLACE:
                                            // If we have folder - folder conflict, nothing needs to be done
                                            if (!(oldDir && newDir)) {
                                                // Delete existing file or folder
                                                try {
                                                    if (oldDir) Utils.deleteDirectory(dest);
                                                    else Files.delete(dest);
                                                } catch (Exception e) {
                                                    exception = e;
                                                    this.file = true;
                                                    exceptionFile = dest.toString();
                                                    exceptionText = "Unable to delete file or folder: " + dest;
                                                    throw new Dialogs.OperationCancelled();
                                                }
                                            }
                                            break;
                                        case Conflict.ConflictResult.RENAME_EXISTING:
                                            // Rename existing file / folder
                                            try {
                                                Files.move(dest, folder.resolve(newName));
                                            } catch (Exception e) {
                                                exception = e;
                                                this.file = true;
                                                exceptionFile = dest + " OR " + newName;
                                                exceptionText = "Unable to rename " + dest + " to " + newName;
                                                throw new Dialogs.OperationCancelled();
                                            }
                                            break;
                                        case Conflict.ConflictResult.RENAME_NEW:
                                            // Rename new file / folder
                                            dest = folder.resolve(newName);
                                            break;
                                    }
                                }
                            } catch (SecurityException e) {
                                exception = e;
                                this.file = true;
                                exceptionFile = dest.toString();
                                throw new Dialogs.OperationCancelled();
                            }
                            // Perform the decryption
                            if (file.isFolder()) {
                                List<TreeDialog.TreeNodeFile> children = file.getChildren();
                                for (TreeDialog.TreeNodeFile child : children) removeFile(dest, child);
                            } else {
                                // Decrypt file
                                try {
                                    manager.decryptFile(dest, file.getFile());
                                } catch (Exception e) {
                                    exception = e;
                                    this.file = true;
                                    exceptionFile = dest + " OR " + file.getFile().getFile();
                                    exceptionText = "Unable to decrypt file: " + file.getFile().name + " into " + dest;
                                    throw new Dialogs.OperationCancelled();
                                }
                                if (copyOrMove == -1 && manager.deleteFiles(Collections.singletonList(file.getFile())) != null) {
                                    runOnFxThreadAndWait(() -> Dialogs.message(stage, "Error", "Unable to delete file: " + file.getFile().getFile()));
                                    throw new Dialogs.OperationCancelled();
                                }
                            }
                        }
                    }.start(false);
                } else {
                    // Ask whether to copy or move the files
                    int copyOrMove = Dialogs.copyOrMove(stage);
                    if (copyOrMove == 0) return;
                    stage.close();
                    List<TreeDialog.TreeNodePath> files = treeNodeResult.getChildren();
                    main.new AsyncAdd<TreeDialog.TreeNodePath>(copyOrMove, files) {
                        @Override
                        void addFile(TreeDialog.TreeNodePath file) throws Dialogs.OperationCancelled {
                            addFile(main.current, file);
                        }

                        // Move a file into the private folder
                        @SuppressWarnings("Duplicates")
                        private void addFile(Manager.Folder folder, TreeDialog.TreeNodePath file) throws Dialogs.OperationCancelled {
                            checkCancelled();
                            // Obtain filename and folder
                            String filename = file.getPath().getFileName().toString();
                            Manager.File f = folder.find(filename);
                            try {
                                // On conflict
                                if (f != null) {
                                    // Perform action based on result
                                    String oldName = filename, newName = Utils.findFilename(filename, s -> folder.find(s) == null);
                                    boolean oldDir = Files.isDirectory(file.getPath()), newDir = f instanceof Manager.Folder;
                                    switch (onConflict(oldName, newName, oldDir, newDir).getResult()) {
                                        case Conflict.ConflictResult.CANCEL:
                                            throw new Dialogs.OperationCancelled();
                                        case Conflict.ConflictResult.SKIP:
                                            return;
                                        case Conflict.ConflictResult.MERGE_OR_REPLACE:
                                            // If we have folder - folder conflict, nothing needs to be done
                                            if (!(oldDir && newDir)) {
                                                // Delete existing file or folder
                                                try {
                                                    manager.deleteFiles(Collections.singletonList(f));
                                                } catch (Exception e) {
                                                    exception = e;
                                                    this.file = true;
                                                    exceptionFile = f.getPrivatePath();
                                                    exceptionText = "Unable to delete file or folder: " + exceptionFile;
                                                    throw new Dialogs.OperationCancelled();
                                                }
                                            }
                                            break;
                                        case Conflict.ConflictResult.RENAME_EXISTING:
                                            // Rename existing file / folder
                                            manager.rename(f, newName);
                                            break;
                                        case Conflict.ConflictResult.RENAME_NEW:
                                            // Rename new file / folder
                                            filename = newName;
                                            break;
                                    }
                                }
                                // Perform the encryption
                                if (file.isFolder()) {
                                    Manager.Folder newFolder = new Manager.Folder(filename, folder);
                                    folder.files.add(newFolder);
                                    // Can't use forEach due to OperationCancelled may be thrown inside lambda
                                    List<TreeDialog.TreeNodePath> paths = file.getChildren();
                                    for (TreeDialog.TreeNodePath path : paths) addFile(newFolder, path);
                                    // Delete the old file if needed
                                    if (copyOrMove == -1) {
                                        try {
                                            // Can't delete right after calling Files.list unless closing the stream
                                            boolean shouldDelete = false;
                                            try (Stream<Path> stream = Files.list(file.getPath())) {
                                                if (stream.count() == 0) shouldDelete = true;
                                            }
                                            if (shouldDelete) Files.delete(file.getPath());
                                        } catch (Exception e) {
                                            exception = e;
                                            this.file = false;
                                            exceptionFile = file.toString();
                                            exceptionText = "Unable to access folder: " + exceptionFile;
                                            throw new Dialogs.OperationCancelled();
                                        }
                                    }
                                } else {
                                    f = new Manager.File(filename, manager.currentUser, folder);
                                    try {
                                        manager.encryptFile(f, file.getPath());
                                        folder.files.add(f);
                                    } catch (Exception e) {
                                        exception = e;
                                        this.file = true;
                                        exceptionFile = file + " OR" + f.name;
                                        exceptionText = "Unable to encrypt file: " + file + " into " + f.name;
                                        throw new Dialogs.OperationCancelled();
                                    }
                                    // Delete the old file if needed
                                    if (copyOrMove == -1) {
                                        try {
                                            Files.delete(file.getPath());
                                        } catch (Exception e) {
                                            exception = e;
                                            this.file = true;
                                            exceptionFile = file.toString();
                                            exceptionText = "Unable to delete file: " + exceptionFile;
                                            throw new Dialogs.OperationCancelled();
                                        }
                                    }
                                }
                            } catch (SecurityException e) {
                                exception = e;
                                this.file = true;
                                exceptionFile = file.toString();
                                throw new Dialogs.OperationCancelled();
                            }
                        }
                    }.start(false);
                }
            }
        }
    }

    @FXML
    public void cancel() {
        stage.close();
    }
}
